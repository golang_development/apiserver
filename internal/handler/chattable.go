package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/service"
	"go.uber.org/zap"
)

type ChattableHandler struct {
	*Handler
	chattableService service.ChattableService
}

func NewChattableHandler(handler *Handler, chattableService service.ChattableService) *ChattableHandler {
	return &ChattableHandler{
		Handler:          handler,
		chattableService: chattableService,
	}
}

//	@Summary	Get Answer from Chat
//	@Schemes
//	@Description.markdown	chattablepost
//	@Tags					Chattable
//	@Accept					json
//	@Produce				json
//	@Security				Bearer
//	@Param					request	body		v1.ChattableInsert	true	"params"
//	@Success				200		{object}	v1.ChatResponseData
//	@Router					/chattable [post]
func (h *ChattableHandler) InsertChattable(ctx *gin.Context) {

	var res v1.ChatResponseData

	var req v1.ChattableInsert
	if err := ctx.ShouldBindJSON(&req); err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}
	res, err := h.chattableService.InsertChattable(req)
	if err != nil {
		h.logger.WithContext(ctx).Error("chattableService.Register error", zap.Error(err))
		v1.HandleError(ctx, http.StatusInternalServerError, err, nil)
		return
	}

	v1.HandleSuccess(ctx, res)

}


//	@Summary	Get All Records from  chattable
//	@Schemes
//	@Description	Get All Records from  chattable
//	@Tags			Chattable
//	@Accept			json
//	@Produce		json
//	@Security		Bearer
//	@Param			limit	query		int	true	"Offset"
//	@Success		200		{object}	[]model.Chattable
//	@Router			/chattable [get]
func (h *ChattableHandler) GetChattable(ctx *gin.Context) {

	limit := ctx.Query("limit")

	limitint, err := strconv.Atoi(limit)
	if err != nil {
		h.logger.WithContext(ctx).Error(" GetChattable error", zap.Error(err))
		v1.HandleError(ctx, http.StatusInternalServerError, err, nil)
		return
	}

	rec, err := h.chattableService.GetChattable(limitint)
	if err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	v1.HandleSuccess(ctx, rec)

}
