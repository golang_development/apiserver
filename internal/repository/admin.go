package repository

import (
	"context"
	"errors"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gorm.io/gorm"
)

type AdminRepository interface {
	FirstById(ctx context.Context, cid string) (*model.User, error)
	GetAllUsers(ctx context.Context) (*v1.GetAllUsersResponce, error)
	GetAllClickstat(ctx context.Context) (*v1.GetClickstatResponce, error)
	GetClickstatById(ctx context.Context, cid string) (*v1.GetClickstatResponce, error)
	BlockUser(ctx context.Context, cid string, block int) error
}

func NewAdminRepository(repository *Repository) AdminRepository {
	return &adminRepository{
		Repository: repository,
	}
}

type adminRepository struct {
	*Repository
}

func (r *adminRepository) BlockUser(ctx context.Context, cid string, block int) error {




	return nil
}



func (r *adminRepository) FirstById(ctx context.Context, cid string) (*model.User, error) {

	var user model.User
	if err := r.DB(ctx).Where("user_id = ?", cid).First(&user).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, v1.ErrNotFound
		}
		return nil, err
	}
	return &user, nil

}

func (r *adminRepository) GetAllUsers(ctx context.Context) (*v1.GetAllUsersResponce, error) {

	var res v1.GetAllUsersResponce

	var users []model.User

	r.DB(ctx).Where("role=?", "user").Find(&users)

	var usrs []v1.UsersResponce

	for _, usr := range users {

		outinv := v1.UsersResponce{UserId: usr.UserId, Nickname: usr.Nickname, Email: usr.Email, Role: usr.Role, Balance: usr.Balance, Block: usr.Block, Verify: usr.Verify,
			Code: usr.Code, Interview: usr.Interview, InterviewPermission: usr.InterviewPermission, Tariff: usr.Tariff, DurationTill: usr.DurationTill, Clicks: usr.Clicks, CreatedAt: usr.CreatedAt, UpdatedAt: usr.UpdatedAt}
		usrs = append(usrs, outinv)

	}

	res.Users = usrs

	return &res, nil

}

func (r *adminRepository) GetAllClickstat(ctx context.Context) (*v1.GetClickstatResponce, error) {
	var res v1.GetClickstatResponce

	var clickstat []model.Clickstat

	var clicksarr []v1.ClickstatResponce

	r.DB(ctx).Find(&clickstat)

	for _, click := range clickstat {

		out := v1.ClickstatResponce{UserId: click.UserId, Clicks: click.Clicks, ClicksLimit: click.ClicksLimit, CreatedAt: click.UpdatedAt}
		clicksarr = append(clicksarr, out)

	}

	res.Clickstat = clicksarr

	return &res, nil

}

func (r *adminRepository) GetClickstatById(ctx context.Context, cid string) (*v1.GetClickstatResponce, error) {
	var res v1.GetClickstatResponce

	var clickstat []model.Clickstat

	var clicksarr []v1.ClickstatResponce

	if err := r.DB(ctx).Where("user_id = ?", cid).Find(&clickstat).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, v1.ErrNotFound
		}
		return nil, err
	}

	for _, click := range clickstat {

		out := v1.ClickstatResponce{UserId: click.UserId, Clicks: click.Clicks, ClicksLimit: click.ClicksLimit, CreatedAt: click.UpdatedAt}
		clicksarr = append(clicksarr, out)

	}
	res.Clickstat = clicksarr
	return &res, nil

}
