package v1

import "time"

type GetClickstatResponce struct {
	Clickstat []ClickstatResponce `json:"clickstat" `
}

type ClickstatResponce struct {
	UserId      string    `json:"userid" example:"1ksksks"`
	Clicks      int       `json:"clicks" example:"13"`
	ClicksLimit int       `json:"clickslimit" example:"70"`
	CreatedAt   time.Time `json:"createdat" example:""`
}
