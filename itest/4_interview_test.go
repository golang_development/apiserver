package itest_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
)

func Skill(s *e2eTestSuite) {

	datastr := `[{"answer":"text","correct":false},{"answer":"text","correct":true}]`

	startPay := v1.InsertInterviewResultRequest{

		Item:              "frontend",
		InterviewJsonData: datastr,
	}

	b, err := json.Marshal(startPay)
	if err != nil {
		s.NoError(err)
	}

	log.Println("!!!SKIL",string(b))

	request, err := http.NewRequest("POST", fmt.Sprintf("%s/v1/insertinterviewsresult", s.ts.URL), bytes.NewBuffer([]byte(b)))
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.bearer)
	if err != nil {

		s.NoError(err)

	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err := io.ReadAll(response.Body)
	if err != nil {
		s.NoError(err)

	}

	s.Equal(200, response.StatusCode)



	request, err = http.NewRequest("GET", fmt.Sprintf("%s/v1/interviewsskill/"+s.cid, s.ts.URL), nil)
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.bearer)
	if err != nil {

		s.NoError(err)

	}

	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err = io.ReadAll(response.Body)
	if err != nil {
		s.NoError(err)

	}

	log.Println(string(sbyteBody))

	s.Equal(200, response.StatusCode)


	s.Run("subTariffs", func() {

		subTariffs(s)

	})

}
