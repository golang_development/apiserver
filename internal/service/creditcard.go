package service

import (
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type CreditcardService interface {
	GetCreditcard(id int64) (*model.Creditcard, error)
}

func NewCreditcardService(service *Service, creditcardRepository repository.CreditcardRepository) CreditcardService {
	return &creditcardService{
		Service:        service,
		creditcardRepository: creditcardRepository,
	}
}

type creditcardService struct {
	*Service
	creditcardRepository repository.CreditcardRepository
}

func (s *creditcardService) GetCreditcard(id int64) (*model.Creditcard, error) {
	return s.creditcardRepository.FirstById(id)
}
