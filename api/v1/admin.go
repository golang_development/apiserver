package v1

import "time"

type GetAllUsersResponce struct {
	Users []UsersResponce `json:"users" `
}

type UsersResponce struct {
	UserId              string    `json:"userid"  example:"AjWXfLwVAT"`
	Nickname            string    `json:"nickname"  example:"vova"`
	Email               string    `json:"email"  example:"test@test.com"`
	Role                string    `json:"role"  example:"user"`
	Balance             float64   `json:"balance"  example:"200"`
	Block               bool      `json:"block"  example:"false"`
	Verify              bool      `json:"verify"  example:"true"`
	Code                int       `json:"code"  example:"72722"`
	Interview           string    `json:"interview"  example:""`
	InterviewPermission bool      `json:"interviewpermission"  example:"true"`
	Tariff              uint      `json:"tariff"  example:"2"`
	DurationTill        time.Time `json:"durationtill"  example:""`
	Clicks              int       `json:"clicks"  example:"70"`
	CreatedAt           time.Time `json:"createdat"  example:""`
	UpdatedAt           time.Time `json:"updatedat"  example:""`
}

type GetUserResponseData struct {
	UserId              string    `json:"userId"  example:"admin1"`
	Nickname            string    `json:"nickname" example:"alan"`
	Email               string    `json:"email" binding:"required,email" example:"nordeasipvip@gmail.com"`
	Password            string    `json:"password"`
	Role                string    `json:"role" example:"admin1"`
	Verify              bool      `json:"verify" example:"false"`
	Interview           string    `json:"interview"  example:"frontend"`
	InterviewPermission bool      `json:"interviewpermission"  example:"true"`
	Tariff              uint      `json:"tariff"  example:"2"`
	DurationTill        time.Time `json:"durationTill"  example:"2024-01-24 03:36:24.954"`
	Clicks              int       `json:"click"  example:"70"`
	Block               bool      `json:"block"  example:"false"`
	CreatedAt           time.Time `json:"createdat"  example:""`
	UpdatedAt           time.Time `json:"updatedat"  example:""`
}
