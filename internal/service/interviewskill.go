package service

import (
	"context"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type InterviewskillService interface {
	GetInterviewskill(ctx context.Context, id string) (*model.Interviewskill, error)
	InsertInterviewResalt(ctx context.Context,cid string, res *v1.InsertInterviewResultRequest) error
}

func NewInterviewskillService(service *Service, interviewskillRepository repository.InterviewskillRepository) InterviewskillService {
	return &interviewskillService{
		Service:                  service,
		interviewskillRepository: interviewskillRepository,
	}
}

type interviewskillService struct {
	*Service
	interviewskillRepository repository.InterviewskillRepository
}

func (s *interviewskillService) GetInterviewskill(ctx context.Context, id string) (*model.Interviewskill, error) {
	return s.interviewskillRepository.FirstById(ctx, id)
}
func (r *interviewskillService) InsertInterviewResalt(ctx context.Context, cid string,res *v1.InsertInterviewResultRequest) error {

	return r.interviewskillRepository.InsertInterviewResalt(ctx, cid,res)

}
