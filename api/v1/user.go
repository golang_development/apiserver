package v1

import "time"

type ForgetPasswordRequest struct {
	Email string `json:"email" binding:"required" example:"nordeasipvip@gmail.com"`
}

type UpdatePasswordRequest struct {
	OldPassword string `json:"oldpassword" binding:"required" example:"password@"`
	NewPassword string `json:"password" binding:"required" example:"password1@"`
}

type RegisterRequest struct {
	Nickname string `json:"nickname" binding:"required" example:"nordea1"`
	Email    string `json:"email" binding:"required" example:"nordeasipvip1@gmail.com"`
	Password string `json:"password" binding:"required" example:"password1@"`
	Invite   string `json:"invite" example:"invite0"`
}
type RegisterResponseData struct {
	UserId              string `json:"userid"  example:"AjWXfLwVAT"`
	Code                int    `json:"code"  example:"55555"`
	Interview           string `json:"interview"  example:"frontend"`
	InterviewPermission bool   `json:"interviewpermission"  example:"true"`
}
type RegisterResponse struct {
	Response
	Data RegisterResponseData
}

type LoginRequest struct {
	Email    string `json:"email" binding:"required" example:"nordeasipvip@gmail.com"`
	Password string `json:"password" binding:"required" example:"password@"`
}
type LoginResponseData struct {
	AccessToken         string `json:"accessToken" example:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVC......" `
	UserId              string `json:"userid" example:"AjWXfLwVAT"`
	Verify              bool   `json:"verify" example:"true" `
	Interview           string `json:"interview"  example:"frontend"`
	InterviewPermission bool   `json:"interviewpermission"  example:"true"`
}
type LoginResponse struct {
	Response
	Data LoginResponseData
}

type UpdateProfileRequest struct {
	Nickname string `json:"nickname" example:"nordea"`
	Email    string `json:"email" binding:"required,email" example:"nordeasipvip@gmail.com"`
	Password string `json:"password"`
	Role     string `json:"role"`
}
type GetProfileResponseData struct {
	UserId              string    `json:"userId"  example:"admin1"`
	Nickname            string    `json:"nickname" example:"alan"`
	Email               string    `json:"email" binding:"required,email" example:"nordeasipvip@gmail.com"`
	Password            string    `json:"password"`
	Role                string    `json:"role" example:"admin1"`
	Verify              bool      `json:"verify" example:"false"`
	Interview           string    `json:"interview"  example:"frontend"`
	InterviewPermission bool      `json:"interviewpermission"  example:"true"`
	Tariff              uint      `json:"tariff"  example:"2"`
	DurationTill        time.Time `json:"durationTill"  example:"2024-01-24 03:36:24.954"`
	Clicks              int       `json:"click"  example:"70"`
}
type GetProfileResponse struct {
	Response
	Data GetProfileResponseData
}
