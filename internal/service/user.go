package service

import (
	"context"

	"reflect"
	"time"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
	"gitlab.com/glass-go/back/apiserver/pkg/generatepassword"
	"gitlab.com/glass-go/back/apiserver/pkg/rangeint"
	"golang.org/x/crypto/bcrypt"
)

type UserService interface {
	Register(ctx context.Context, req *v1.RegisterRequest) (v1.RegisterResponseData, error)
	Login(ctx context.Context, req *v1.LoginRequest) (v1.LoginResponseData, error)
	GetProfile(ctx context.Context, userId string) (*v1.GetProfileResponseData, error)
	UpdateProfile(ctx context.Context, userId string, req *v1.UpdateProfileRequest) error
	BlockProfile(ctx context.Context, cid string, block int) error
	UpdatePassword(ctx context.Context, userId string, req *v1.UpdatePasswordRequest) error
	ForgotPassword(ctx context.Context, req *v1.ForgetPasswordRequest) error
}

func NewUserService(service *Service, userRepo repository.UserRepository) UserService {
	return &userService{
		userRepo: userRepo,
		Service:  service,
	}
}

type userService struct {
	userRepo   repository.UserRepository
	tariffRepo repository.TariffRepository
	*Service
}

func (s *userService) Register(ctx context.Context, req *v1.RegisterRequest) (v1.RegisterResponseData, error) {
	var res v1.RegisterResponseData
	var inerr error
	var exists bool

	inv := &model.Invite{Item: "noinvite", Permission: true}

	val := reflect.ValueOf(req).Elem()

	field := val.FieldByName("Invite").String()

	tariff := model.Tariff{}

	if len(field) > 0 {

		inv, exists, inerr = s.userRepo.CheckInvite(ctx, req.Invite)
		if inerr != nil {

			return res, v1.ErrInviteAlreadyUse
		}

		if exists {

			return res, v1.ErrInviteAlreadyUse
		}

		tariff.Clicklimit = 20
		tariff.ID = 5
		tariff.Duration = 0

	}

	emailexit, err := s.userRepo.CheckEmail(ctx, req.Email)

	if err != nil {

		return res, err

	}

	if emailexit {

		return res, v1.ErrEmailAlreadyUse
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return res, err
	}
	// Generate user ID
	userId, err := s.sid.GenString()
	if err != nil {
		return res, err
	}
	code := rangeint.RangeIn(10000, 99999)

	var user *model.User

	if tariff.Clicklimit == 0 {

		user = &model.User{
			UserId:              userId,
			Nickname:            req.Nickname,
			Email:               req.Email,
			Password:            string(hashedPassword),
			Code:                code,
			Interview:           inv.Item,
			InterviewPermission: inv.Permission,
			DurationTill:        time.Now(),
		}

	} else {

		user = &model.User{
			UserId:              userId,
			Nickname:            req.Nickname,
			Email:               req.Email,
			Password:            string(hashedPassword),
			Code:                code,
			Interview:           inv.Item,
			InterviewPermission: inv.Permission,
			DurationTill:        time.Now(),
			Tariff:              tariff.ID,
			Clicks:              tariff.Clicklimit,
		}

	}

	// Transaction demo
	err = s.tm.Transaction(ctx, func(ctx context.Context) error {
		// Create a user
		if err = s.userRepo.Create(ctx, user); err != nil {
			return err
		}
		// TODO: other repo
		return nil
	})

	res.UserId = userId
	res.Code = code
	res.Interview = inv.Item
	res.InterviewPermission = inv.Permission
	return res, nil
}

func (s *userService) Login(ctx context.Context, req *v1.LoginRequest) (v1.LoginResponseData, error) {
	var res v1.LoginResponseData
	user, err := s.userRepo.GetByEmail(ctx, req.Email)
	if err != nil || user == nil {

		return res, v1.ErrUnauthorized
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password))
	if err != nil {

		return res, err
	}

	token, err := s.jwt.GenToken(user.UserId, user.Role,time.Now().Add(time.Hour*24*90))
	if err != nil {
		return res, err
	}

	res.AccessToken = token
	res.UserId = user.UserId
	res.Verify = user.Verify
	res.Interview = user.Interview
	res.InterviewPermission = user.InterviewPermission

	return res, nil
}

func (s *userService) GetProfile(ctx context.Context, userId string) (*v1.GetProfileResponseData, error) {
	user, err := s.userRepo.GetByID(ctx, userId)
	if err != nil {
		return nil, err
	}

	return &v1.GetProfileResponseData{
		UserId:              user.UserId,
		Nickname:            user.Nickname,
		Email:               user.Email,
		Role:                user.Role,
		Password:            user.Password,
		Verify:              user.Verify,
		Interview:           user.Interview,
		InterviewPermission: user.InterviewPermission,
		Tariff:              user.Tariff,
		DurationTill:        user.DurationTill,
		Clicks:              user.Clicks,
	}, nil
}

func (s *userService) UpdateProfile(ctx context.Context, userId string, req *v1.UpdateProfileRequest) error {
	user, err := s.userRepo.GetByID(ctx, userId)
	if err != nil {
		return err
	}

	user.Email = req.Email
	user.Nickname = req.Nickname
	user.Role = req.Role

	if err = s.userRepo.Update(ctx, user); err != nil {
		return err
	}

	return nil
}

func (s *userService) BlockProfile(ctx context.Context, cid string, block int) error {

	if err := s.userRepo.BlockProfile(ctx, cid, block); err != nil {
		return err
	}

	return nil

}

func (s *userService) UpdatePassword(ctx context.Context, userId string, req *v1.UpdatePasswordRequest) error {

	user, err := s.userRepo.GetByID(ctx, userId)
	if err != nil || user == nil {

		return err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.OldPassword))
	if err != nil {

		return err
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(req.NewPassword), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	if err := s.userRepo.UpdatePassword(ctx, userId, string(hashedPassword)); err != nil {
		return err
	}

	return nil
}

func (s *userService) ForgotPassword(ctx context.Context, req *v1.ForgetPasswordRequest) error {

	gennewpass := generatepassword.GeneratePassword(12, true, true, true)

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(gennewpass), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	if err := s.userRepo.ForgotPassword(ctx, req.Email, gennewpass, string(hashedPassword)); err != nil {
		return err
	}

	return nil
}
