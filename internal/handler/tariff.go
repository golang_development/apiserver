package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type TariffHandler struct {
	*Handler
	tariffService service.TariffService
}

func NewTariffHandler(handler *Handler, tariffService service.TariffService) *TariffHandler {
	return &TariffHandler{
		Handler:       handler,
		tariffService: tariffService,
	}
}


// GetTarifs godoc
//	@Summary	GetTariffs
//	@Schemes
//	@Description	GetTariffs
//	@Tags			Tariffs
//	@Accept			json
//	@Produce		json
//	@Security		Bearer
//	@Success		200	{object}	v1.GetTariffsResponce
//	@Router			/tariffs [get]
func (h *TariffHandler) GetTariffs(ctx *gin.Context) {

	tariffs, err := h.tariffService.GetTariffs(ctx)
	if err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	v1.HandleSuccess(ctx, tariffs)

}
