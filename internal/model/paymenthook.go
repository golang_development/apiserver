package model

import "gorm.io/gorm"

type Invoice struct {
	gorm.Model
	InvId         string
	UserId        string `gorm:"not null"`
	Description   string `gorm:"not null"`
	OutSum        float64
	Crc           string
	PaymentMethod string
	IncSum        float64
	IncCurrLabel  string
	IsTest        bool
	EMail         string
	Fee           float64
	Tariff        uint
}

func (m *Invoice) TableName() string {
	return "invoice"
}
