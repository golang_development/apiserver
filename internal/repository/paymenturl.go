package repository

import (
	"context"
	"strconv"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"

	"gitlab.com/glass-go/back/apiserver/pkg/payments/robokassa"
)

type PaymenturlRepository interface {
	GetInvoicesByClid(ctx context.Context, userid string) (v1.GetInvoicesByClidResponce, error)
	GetPaymentUrl(ctx context.Context, userid string, req v1.GetPaymenturlRequest) (string, error)
}

func NewPaymenturlRepository(repository *Repository) PaymenturlRepository {
	return &paymenturlRepository{
		Repository: repository,
	}
}

type paymenturlRepository struct {
	*Repository
}

func (r *paymenturlRepository) GetInvoicesByClid(ctx context.Context, userid string) (v1.GetInvoicesByClidResponce, error) {

	var res v1.GetInvoicesByClidResponce
	var dbres []model.Invoice

	r.DB(ctx).Where("user_id =?", userid).Find(&dbres)

	var invs []v1.InvoicesByClidResponce

	for _, inv := range dbres {

		tarstr := strconv.FormatUint(uint64(inv.Tariff), 10)

		outinv := v1.InvoicesByClidResponce{InvId: inv.InvId, Description: inv.Description, IncSum: inv.IncSum, Tariff: tarstr}
		invs = append(invs, outinv)

	}

	res.Invoices = invs

	return res, nil

}

func (r *paymenturlRepository) GetPaymentUrl(ctx context.Context, userid string, req v1.GetPaymenturlRequest) (string, error) {

	u, _ := strconv.ParseUint(req.Tariff, 10, 32)

	invoice := model.Invoice{UserId: userid, Description: req.Description, Tariff: uint(u)}

	result := r.DB(ctx).Create(&invoice)

	if result.Error != nil {

		return "", result.Error
	}

	var checkMod model.Invoice
	result.Last(&checkMod)

	invId := strconv.FormatUint(uint64(checkMod.ID), 10)

	url, err := robokassa.CreatePaymentURL(r.robo.Urlstr, r.robo.MerchantLogin, r.robo.Password1, invId, req.Description, req.Amount, r.robo.IsTest)
	if err != nil {
		return "", err
	}

	return url, nil
}
