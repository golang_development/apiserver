package repository

import (
	"context"
	"errors"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gorm.io/gorm"
)

type InterviewskillRepository interface {
	FirstById(ctx context.Context, id string) (*model.Interviewskill, error)
	InsertInterviewResalt(ctx context.Context, cid string, res *v1.InsertInterviewResultRequest) error
}

func NewInterviewskillRepository(repository *Repository) InterviewskillRepository {
	return &interviewskillRepository{
		Repository: repository,
	}
}

type interviewskillRepository struct {
	*Repository
}

func (r *interviewskillRepository) FirstById(ctx context.Context, id string) (*model.Interviewskill, error) {
	var interviewskill model.Interviewskill
	// TODO: query db
	if err := r.DB(ctx).Where("user_id = ?", id).First(&interviewskill).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, err
	}

	return &interviewskill, nil
}

func (r *interviewskillRepository) InsertInterviewResalt(ctx context.Context, cid string, res *v1.InsertInterviewResultRequest) error {

	skill := model.Interviewskill{UserId: cid, Item: res.Item, JsonResult: res.InterviewJsonData}

	r.db.Create(&skill)

	r.db.Model(&model.User{}).Where("user_id = ?", cid).Update("InterviewPermission", false)

	return nil
}
