package v1

type ChattableInsert struct {
	UserId  string `json:"userid" binding:"required" example:"tester"`
	Mlmodel string `json:"mlmodel" binding:"required" example:"chatgpt"`
	Ask     string `json:"ask" binding:"required" example:"What do you think about Russia?"`
	// Answer  string `json:"answer" binding:"required" example:"Russia very big country"`
}
type ChatResponseData struct {
	Answer string `json:"answer" example:"Finland good for old man!"` 
}