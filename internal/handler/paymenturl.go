package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type PaymenturlHandler struct {
	*Handler
	paymenturlService service.PaymenturlService
}

func NewPaymenturlHandler(handler *Handler, paymenturlService service.PaymenturlService) *PaymenturlHandler {
	return &PaymenturlHandler{
		Handler:           handler,
		paymenturlService: paymenturlService,
	}
}

// GetPaymenturl godoc
//	@Summary	GetInvoicesByClid
//	@Schemes
//	@Description	GetInvoicesByClid
//	@Tags			Payment
//	@Accept			json
//	@Produce		json
//	@Security		Bearer
//	@Success		200	{object}	v1.GetInvoicesByClidResponce
//	@Router			/getinvoicesbyclid [get]
func (h *PaymenturlHandler) GetInvoicesByClid(ctx *gin.Context) {

	userId := GetUserIdFromCtx(ctx)
	if userId == "" {
		v1.HandleError(ctx, http.StatusUnauthorized, v1.ErrUnauthorized, nil)
		return
	}

	invoices, err := h.paymenturlService.GetInvoicesByClid(ctx, userId)
	if err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	v1.HandleSuccess(ctx, invoices)

}

// GetPaymenturl godoc
//	@Summary	GetPaymenturl
//	@Schemes
//	@Description	GetPaymenturl
//	@Tags			Payment
//	@Accept			json
//	@Produce		json
//	@Security		Bearer
//	@Param			request	body		v1.GetPaymenturlRequest	true	"params"
//	@Success		200		{object}	v1.Response
//	@Router			/paymenturl [post]
func (h *PaymenturlHandler) GetPaymenturl(ctx *gin.Context) {

	userId := GetUserIdFromCtx(ctx)
	if userId == "" {
		v1.HandleError(ctx, http.StatusUnauthorized, v1.ErrUnauthorized, nil)
		return
	}

	var req v1.GetPaymenturlRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	urlstr, err := h.paymenturlService.GetPaymenturl(ctx, userId, req)
	if err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	v1.HandleSuccess(ctx, urlstr)

}
