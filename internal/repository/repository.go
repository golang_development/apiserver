package repository

import (
	"context"
	"fmt"
	"log/slog"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/kalbhor/tasqueue/v2"
	rb "github.com/kalbhor/tasqueue/v2/brokers/redis"
	rr "github.com/kalbhor/tasqueue/v2/results/redis"
	"github.com/redis/go-redis/v9"
	"github.com/spf13/viper"
	"gitlab.com/glass-go/back/apiserver/pkg/log"
	"gitlab.com/glass-go/back/apiserver/pkg/payments/robokassa"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"moul.io/zapgorm2"
)

const ctxTxKey = "TxKey"

type TaskQue struct {
	Sqlurls  string
	Tasqueue *tasqueue.Server
}

type Repository struct {
	db       *gorm.DB
	rdb      *redis.Client
	rcl      *resty.Client
	robo     *robokassa.Client
	tasqueue TaskQue

	logger *log.Logger
}

func NewRepository(db *gorm.DB, rdb *redis.Client, rcl *resty.Client, logger *log.Logger, robo *robokassa.Client, tasqueue TaskQue) *Repository {
	return &Repository{
		db:       db,
		rdb:      rdb,
		rcl:      rcl,
		logger:   logger,
		robo:     robo,
		tasqueue: tasqueue,
	}
}

func NewTasqueue(conf *viper.Viper) TaskQue {

	rdbaddr := conf.GetString("data.redis.addr")
	rdbpass := conf.GetString("data.redis.password")

	lo := slog.Default()
	srv, err := tasqueue.NewServer(tasqueue.ServerOpts{
		Broker: rb.New(rb.Options{
			Addrs:    []string{rdbaddr},
			Password: rdbpass,
			DB:       0,
		}, lo),
		Results: rr.New(rr.Options{
			Addrs:      []string{rdbaddr},
			Password:   rdbpass,
			DB:         0,
			MetaExpiry: time.Second * 30,
		}, lo),
		Logger: lo.Handler(),
	})
	if err != nil {

		lo.Error(err.Error())
	}

	env := conf.GetString("env")
	var sqlurl string
	if env == "local" {

		sqlurl = "root:123456@tcp(glassgo-db)/glassgodb?charset=utf8mb4&parseTime=True&loc=Local"

	} else {

		sqlurl = conf.GetString("data.mysql.user")

	}

	return TaskQue{Sqlurls: sqlurl, Tasqueue: srv}

}

func NewRobo(conf *viper.Viper) *robokassa.Client {

	var client robokassa.Client
	client.Set(conf.GetString("payments.robokassa.urlstr"), conf.GetString("payments.robokassa.merchantlogin"), conf.GetString("payments.robokassa.password1"), conf.GetString("payments.robokassa.password2"), conf.GetString("payments.robokassa.istest"))

	return &client

}

func NewResty(conf *viper.Viper) *resty.Client {

	client := resty.New()
	client.SetAuthToken(conf.GetString("ml.chatgpt.apiKey"))

	return client
}

type Transaction interface {
	Transaction(ctx context.Context, fn func(ctx context.Context) error) error
}

func NewTransaction(r *Repository) Transaction {
	return r
}

// DB return tx
// If you need to create a Transaction, you must call DB(ctx) and Transaction(ctx,fn)
func (r *Repository) DB(ctx context.Context) *gorm.DB {
	v := ctx.Value(ctxTxKey)
	if v != nil {
		if tx, ok := v.(*gorm.DB); ok {
			return tx
		}
	}
	return r.db.WithContext(ctx)
}

func (r *Repository) Transaction(ctx context.Context, fn func(ctx context.Context) error) error {
	return r.db.WithContext(ctx).Transaction(func(tx *gorm.DB) error {
		ctx = context.WithValue(ctx, ctxTxKey, tx)
		return fn(ctx)
	})
}

func NewDB(conf *viper.Viper, l *log.Logger) *gorm.DB {
	logger := zapgorm2.New(l.Logger)
	logger.SetAsDefault()
	db, err := gorm.Open(mysql.Open(conf.GetString("data.mysql.user")), &gorm.Config{Logger: logger})
	if err != nil {
		panic(err)
	}
	db = db.Debug()
	return db
}
func NewRedis(conf *viper.Viper) *redis.Client {
	rdb := redis.NewClient(&redis.Options{
		Addr:     conf.GetString("data.redis.addr"),
		Password: conf.GetString("data.redis.password"),
		DB:       conf.GetInt("data.redis.db"),
	})

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err := rdb.Ping(ctx).Result()
	if err != nil {
		panic(fmt.Sprintf("redis error: %s", err.Error()))
	}

	return rdb
}
