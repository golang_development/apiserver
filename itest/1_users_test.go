package itest_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
)

func Users(s *e2eTestSuite) {

	request, err := http.NewRequest("GET", fmt.Sprintf("%s/v1/user", s.ts.URL), nil)
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.bearer)
	if err != nil {

		s.NoError(err)

	}
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err := io.ReadAll(response.Body)
	if err != nil {
		s.NoError(err)

	}

	log.Println(string(sbyteBody))


	////////forgotpassword////////

	jsonData := &v1.ForgetPasswordRequest{
		
		Email:    s.email,
	}

	b, err := json.Marshal(jsonData)
	if err != nil {
		s.NoError(err)
	}


	request, err = http.NewRequest("POST", fmt.Sprintf("%s/v1/forgotpassword", s.ts.URL), bytes.NewBuffer(b))
	request.Header.Set("Content-Type", "application/json;")
	if err != nil {

		s.NoError(err)

	}
	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err = io.ReadAll(response.Body)
	if err != nil {
		s.NoError(err)

	}

	log.Println(string(sbyteBody))

	s.Equal(200, response.StatusCode)





	s.Run("subChattableUsers", func() {

		Chattable(s)

	})

}
