package handler

import (

	"net/http"

	"github.com/gin-gonic/gin"
	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type AdminHandler struct {
	*Handler
	adminService service.AdminService
}

func NewAdminHandler(handler *Handler, adminService service.AdminService) *AdminHandler {
	return &AdminHandler{
		Handler:      handler,
		adminService: adminService,
	}
}

// @Summary	GetClickstatById
// @Schemes
// @Description	GetClickstatById
// @Tags			Admin
// @Accept			json
// @Produce		json
// @Security		Bearer
// @Param			cid	query		string	true	"cid"
// @Success		200	{object}	v1.GetClickstatResponce
// @Router			/admin/clickstatbyid [get]
func (h *AdminHandler) GetClickstatById(ctx *gin.Context) {

	cid := ctx.Query("cid")

	res, err := h.adminService.GetClickstatById(ctx, cid)
	if err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	v1.HandleSuccess(ctx, res)

}

// @Summary	GetClickstat
// @Schemes
// @Description	GetClickstat
// @Tags			Admin
// @Accept			json
// @Produce		json
// @Security		Bearer
// @Success		200	{object}	v1.GetClickstatResponce
// @Router			/admin/clickstat [get]
func (h *AdminHandler) GetClickstat(ctx *gin.Context) {

	res, err := h.adminService.GetAllClickstat(ctx)
	if err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	v1.HandleSuccess(ctx, res)

}

// @Summary	GetAllUsers
// @Schemes
// @Description	GetAllUsers
// @Tags			Admin
// @Accept			json
// @Produce		json
// @Security		Bearer
// @Success		200	{object}	v1.GetAllUsersResponce
// @Router			/admin/allusers [get]
func (h *AdminHandler) GetAllUsers(ctx *gin.Context) {

	// role := GetUserRoleFromCtx(ctx)
	// if role == "" {
	// 	v1.HandleError(ctx, http.StatusUnauthorized, v1.ErrUnauthorized, nil)
	// 	return
	// }

	// log.Println("!!!ROLE", role)

	// if role != "admin" {

	// 	v1.HandleError(ctx, http.StatusUnauthorized, v1.ErrUnauthorized, nil)
	// 	return

	// }

	res, err := h.adminService.GetAllUsers(ctx)
	if err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	v1.HandleSuccess(ctx, res)

}

// @Summary	GetUserById
// @Schemes
// @Description	GetUserById
// @Tags			Admin
// @Accept			json
// @Produce		json
// @Security		Bearer
// @Param			cid	query		string	true	"cid"
// @Success		200	{object}	v1.GetUserResponseData
// @Router			/admin/user [get]
func (h *AdminHandler) GetUserById(ctx *gin.Context) {

	cid := ctx.Query("cid")

	res, err := h.adminService.GetUserById(ctx, cid)
	if err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	v1.HandleSuccess(ctx, res)
}
