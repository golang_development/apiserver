package service

import (
	"context"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type TariffService interface {
	GetTariffs(ctx context.Context) (*v1.GetTariffsResponce, error)
}

func NewTariffService(service *Service, tariffRepository repository.TariffRepository) TariffService {
	return &tariffService{
		Service:        service,
		tariffRepository: tariffRepository,
	}
}

type tariffService struct {
	*Service
	tariffRepository repository.TariffRepository
}

func (s *tariffService) GetTariffs(ctx context.Context) (*v1.GetTariffsResponce, error) {
	return s.tariffRepository.GetTariffs(ctx)
}
