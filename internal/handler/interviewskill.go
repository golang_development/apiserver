package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type InterviewskillHandler struct {
	*Handler
	interviewskillService service.InterviewskillService
}

func NewInterviewskillHandler(handler *Handler, interviewskillService service.InterviewskillService) *InterviewskillHandler {
	return &InterviewskillHandler{
		Handler:               handler,
		interviewskillService: interviewskillService,
	}
}

// GetInterviewskill godoc
//	@Summary	GetInterviewskill get test result
//	@Schemes
//	@Description	GetInterviewskill get test result
//	@Tags			Interviews
//	@Accept			json
//	@Produce		json
//	@Security		Bearer
//	@Param			id	path		string	true	"UserId"
//	@Success		200	{object}	model.Interviewskill
//	@Router			/interviewsskill/{id} [get]
func (h *InterviewskillHandler) GetInterviewskill(ctx *gin.Context) {

	id := ctx.Param("id")

	skill, err := h.interviewskillService.GetInterviewskill(ctx, id)
	if err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	v1.HandleSuccess(ctx, skill)

}

// InserInterviewsresult godoc
//	@Summary	InserInterviewsresult insert test result
//	@Schemes
//	@Description	InserInterviewsresult insert test result
//	@Tags			Interviews
//	@Accept			json
//	@Produce		json
//	@Security		Bearer
//	@Param			request	body		v1.InsertInterviewResultRequest	true	"params"
//	@Success		200		{object}	v1.Response
//	@Router			/insertinterviewsresult [post]
func (h *InterviewskillHandler) InserInterviewsresult(ctx *gin.Context) {

	userId := GetUserIdFromCtx(ctx)
	if userId == "" {
		v1.HandleError(ctx, http.StatusUnauthorized, v1.ErrUnauthorized, nil)
		return
	}

	var req v1.InsertInterviewResultRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	err := h.interviewskillService.InsertInterviewResalt(ctx, userId, &req)
	if err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	v1.HandleSuccess(ctx, nil)

}
