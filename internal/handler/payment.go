package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type PaymentHandler struct {
	*Handler
	paymentService service.PaymentService
}

func NewPaymentHandler(handler *Handler, paymentService service.PaymentService) *PaymentHandler {
	return &PaymentHandler{
		Handler:      handler,
		paymentService: paymentService,
	}
}

func (h *PaymentHandler) GetPayment(ctx *gin.Context) {

}
