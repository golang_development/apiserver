package itest_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
)

func Paymneturl(s *e2eTestSuite) {

	startPay := v1.GetPaymenturlRequest{

		MerchantLogin: "glsgo",
		Amount:        100,
		Description:   "payment for ChatGp",
		IsTest:        "0",
		Tariff:        "4",
	}

	b, err := json.Marshal(startPay)
	if err != nil {
		s.NoError(err)
	}

	request, err := http.NewRequest("POST", fmt.Sprintf("%s/v1/paymenturl", s.ts.URL), bytes.NewBuffer([]byte(b)))
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.bearer)
	if err != nil {

		s.NoError(err)

	}
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err := io.ReadAll(response.Body)
	if err != nil {
		s.NoError(err)

	}

	log.Println(string(sbyteBody))

	s.Equal(200, response.StatusCode)

	/// HOOK

	// request, err = http.NewRequest("GET", fmt.Sprintf("%s/v1/paymenthook?out_summ=1.000000&OutSum=1.000000&inv_id=1&InvId=1&crc=95103C6F747B661E0DBB44AD50CAE637F7B4370635E2DA1316BADD8D308C177A7F342C4D98BA912CF387D89D40CEBB4F2B5ED34B84920E71B8346AFB823C626E&SignatureValue=95103C6F747B661E0DBB44AD50CAE637F7B4370635E2DA1316BADD8D308C177A7F342C4D98BA912CF387D89D40CEBB4F2B5ED34B84920E71B8346AFB823C626E&PaymentMethod=BankCard&IncSum=1.000000&IncCurrLabel=BankCardPSR&EMail=alexander.emelyanov@gmail.com&Fee=0.040000", s.ts.URL), nil)
	// request.Header.Set("Content-Type", "application/json;")
	// request.Header.Set("Authorization", "Bearer "+s.bearer)
	// if err != nil {

	// 	s.NoError(err)

	// }
	// response, err = http.DefaultClient.Do(request)
	// if err != nil {
	// 	s.NoError(err)

	// }

	// sbyteBody, err = io.ReadAll(response.Body)
	// if err != nil {
	// 	s.NoError(err)

	// }

	// log.Println("----------------")
	// log.Println(string(sbyteBody))
	// log.Println("----------------")

	// s.Equal(200, response.StatusCode)



	//// get Invoices
	request, err = http.NewRequest("GET", fmt.Sprintf("%s/v1/getinvoicesbyclid", s.ts.URL), nil)
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.bearer)
	if err != nil {

		s.NoError(err)

	}
	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err = io.ReadAll(response.Body)
	if err != nil {
		s.NoError(err)

	}

	log.Println("----------------")
	log.Println(string(sbyteBody))
	log.Println("----------------")

	s.Equal(200, response.StatusCode)

	s.Run("subChattableUsers", func() {

		Skill(s)

	})

}
