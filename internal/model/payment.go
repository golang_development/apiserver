package model

import (
	"time"
)

type Payment struct {
	UserId     string  `gorm:"unique;not null"`
	Payment    string  `gorm:"default:null"`
	CreditCard string  `gorm:"default:null"`
	Amount     float64 `gorm:"unique;not null"`
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

func (m *Payment) TableName() string {
	return "payments"
}

