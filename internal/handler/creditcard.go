package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type CreditcardHandler struct {
	*Handler
	creditcardService service.CreditcardService
}

func NewCreditcardHandler(handler *Handler, creditcardService service.CreditcardService) *CreditcardHandler {
	return &CreditcardHandler{
		Handler:      handler,
		creditcardService: creditcardService,
	}
}

func (h *CreditcardHandler) GetCreditcard(ctx *gin.Context) {

}
