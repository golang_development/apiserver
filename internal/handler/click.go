package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type ClickHandler struct {
	*Handler
	clickService service.ClickService
}

func NewClickHandler(handler *Handler, clickService service.ClickService) *ClickHandler {
	return &ClickHandler{
		Handler:      handler,
		clickService: clickService,
	}
}


// GetClickt godoc
//	@Summary	GetClick
//	@Schemes
//	@Description	GetClick
//	@Tags			Click
//	@Accept			json
//	@Produce		json
//	@Security		Bearer
//	@Success		200	{object}	v1.ClicksResponce
//	@Router			/clicks [get]
func (h *ClickHandler) GetClick(ctx *gin.Context) {

	userId := GetUserIdFromCtx(ctx)
	if userId == "" {
		v1.HandleError(ctx, http.StatusUnauthorized, v1.ErrUnauthorized, nil)
		return
	}

	res, err := h.clickService.GetClick(ctx, userId)
	if err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	v1.HandleSuccess(ctx, res)

}
