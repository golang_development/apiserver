package robokassa

import (
	"crypto/sha512"
	"encoding/hex"

	"errors"
	"net/url"
	"strconv"
	"strings"
)

var ErrOnlyIntegerRub = errors.New("only integer rub allowed")

type Client struct {
	Urlstr        string
	MerchantLogin string
	Password1     string
	Password2     string
	IsTest        string
}

func (c *Client) Set(Urlstr string, MerchantLogin string, Password1 string, Password2 string, IsTest string) *Client {
	c.Urlstr = Urlstr
	c.MerchantLogin = MerchantLogin
	c.Password1 = Password1
	c.Password2 = Password2
	c.IsTest = IsTest
	return c
}

func CreatePaymentURL(urlstr string, merchantLogin string, password1 string, cid string, description string, amount uint32, istest string) (paymentURL string, err error) {
	if !strings.HasSuffix(strconv.Itoa(int(amount)), "00") {
		err = ErrOnlyIntegerRub
		return
	}

	outSum := strconv.Itoa(int(amount / 100))

	sha := sha512.New()
	_, err = sha.Write([]byte(strings.Join([]string{
		merchantLogin,
		outSum,
		cid,
		password1,
	}, ":")))
	if err != nil {
		return
	}

	hash := strings.ToUpper(hex.EncodeToString(sha.Sum(nil)))

	u, err := url.Parse(urlstr)
	if err != nil {
		return
	}

	q := u.Query()
	q.Set("MerchantLogin", merchantLogin)
	q.Set("OutSum", outSum)
	q.Set("InvId", cid)
	q.Set("Description", description)
	q.Set("SignatureValue", hash)
	q.Set("Culture", "ru")
	q.Set("IsTest", istest)

	u.RawQuery = q.Encode()

	paymentURL = u.String()

	// log.Println("!!!!URL", paymentURL)
	return
}
