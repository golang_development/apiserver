package model

import "gorm.io/gorm"

type Click struct {
	gorm.Model
}

func (m *Click) TableName() string {
    return "click"
}
