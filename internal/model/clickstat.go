package model

import "gorm.io/gorm"

type Clickstat struct {
	gorm.Model
	UserId      string `gorm:"not null"`
	Clicks      int    `gorm:"not null"`
	ClicksLimit int    `gorm:"not null"`
}

func (m *Clickstat) TableName() string {
	return "clickstat"
}
