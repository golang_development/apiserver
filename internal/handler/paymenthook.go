package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type PaymenthookHandler struct {
	*Handler
	paymenthookService service.PaymenthookService
}

func NewPaymenthookHandler(handler *Handler, paymenthookService service.PaymenthookService) *PaymenthookHandler {
	return &PaymenthookHandler{
		Handler:            handler,
		paymenthookService: paymenthookService,
	}
}

func (h *PaymenthookHandler) NotPaid(ctx *gin.Context) {

	inv_id := ctx.Query("inv_id")

	out_summ := ctx.Query("out_summ")

	out_summ_float64, err := strconv.ParseFloat(out_summ,64)
	if err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}
	crc := ctx.Query("crc")

	paymentMethod := ctx.Query("PaymentMethod")

	incSum := ctx.Query("IncSum")
	incSum_float64, err := strconv.ParseFloat(incSum,64)
	if err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	incCurrLabel := ctx.Query("IncCurrLabel")

	isTest := ctx.Query("IsTest")
	var isTestBool bool

	if isTest == "1" {
		isTestBool = true

	}

	fee := ctx.Query("Fee")

	fee_flow, err := strconv.ParseFloat(fee, 64)
	if err != nil {

		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return

	}

	inv_id = "1" ///Only for test
	payment := model.Invoice{InvId: inv_id, OutSum: out_summ_float64, Crc: crc, PaymentMethod: paymentMethod, IncSum: incSum_float64, IncCurrLabel: incCurrLabel, IsTest: isTestBool, Fee: fee_flow}

	if err := h.paymenthookService.UpdateInvoice(ctx, &payment); err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	v1.HandleSuccess(ctx, nil)
}

func (h *PaymenthookHandler) GetPaymenthook(ctx *gin.Context) {

	inv_id := ctx.Query("inv_id")

	out_summ := ctx.Query("out_summ")

	out_summ_float64, err := strconv.ParseFloat(out_summ,64)
	if err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}
	crc := ctx.Query("crc")

	paymentMethod := ctx.Query("PaymentMethod")

	incSum := ctx.Query("IncSum")
	incSum_float64, err := strconv.ParseFloat(incSum,64)
	if err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	incCurrLabel := ctx.Query("IncCurrLabel")

	isTest := ctx.Query("IsTest")
	var isTestBool bool

	if isTest == "1" {
		isTestBool = true

	}

	fee := ctx.Query("Fee")

	fee_flow, err := strconv.ParseFloat(fee, 64)
	if err != nil {

		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return

	}

	payment := model.Invoice{InvId: inv_id, OutSum: out_summ_float64, Crc: crc, PaymentMethod: paymentMethod, IncSum: incSum_float64, IncCurrLabel: incCurrLabel, IsTest: isTestBool, Fee: fee_flow}

	if err := h.paymenthookService.UpdateInvoice(ctx, &payment); err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	v1.HandleSuccess(ctx, nil)
}

// /v1/paymenthook?out_summ=100&OutSum=100&inv_id=678678&InvId=678678&crc=4195AAB1486A91BD6E32D102EA697A07064F532A69D686AA72F85C3B028F25FA&SignatureValue=4195AAB1486A91BD6E3F25FA&PaymentMethod=BankCard&IncSum=100&IncCurrLabel=BankCardPSR&IsTest=1&EMail=&Fee=0.0"

// /v1/paymenthook?out_summ=1.000000&OutSum=1.000000&inv_id=19&InvId=19&crc=95103C6F747B661E0DBB44AD50CAE637F7B4370635E2DA1316BADD8D308C177A7F342C4D98BA912CF387D89D40CEBB4F2B5ED34B84920E71B8346AFB823C626E&SignatureValue=95103C6F747B661E0DBB44AD50CAE637F7B4370635E2DA1316BADD8D308C177A7F342C4D98BA912CF387D89D40CEBB4F2B5ED34B84920E71B8346AFB823C626E&PaymentMethod=BankCard&IncSum=1.000000&IncCurrLabel=BankCardPSR&EMail=alexander.emelyanov@gmail.com&Fee=0.040000"