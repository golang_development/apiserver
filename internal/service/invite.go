package service

import (
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type InviteService interface {
	GetInvite(id int64) (*model.Invite, error)
}

func NewInviteService(service *Service, inviteRepository repository.InviteRepository) InviteService {
	return &inviteService{
		Service:        service,
		inviteRepository: inviteRepository,
	}
}

type inviteService struct {
	*Service
	inviteRepository repository.InviteRepository
}

func (s *inviteService) GetInvite(id int64) (*model.Invite, error) {
	return s.inviteRepository.FirstById(id)
}
