package common

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/pkg/config"
	clog "gitlab.com/glass-go/back/apiserver/pkg/log"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"moul.io/zapgorm2"
)

func SetUpDB() error {

	fmt.Println("beginSetUpDB")
	err := os.Setenv("APP_CONF", "../config/local.yml")
	if err != nil {
		fmt.Println("Setenv error", err)
	}
	var envConf = flag.String("conf", "config/local.yml", "config path, eg: -conf ./config/local.yml")
	flag.Parse()
	conf := config.NewConfig(*envConf)

	// log.Println("conf", conf)
	l := clog.NewLog(conf)

	logger := zapgorm2.New(l.Logger)
	logger.SetAsDefault()

	db, err := gorm.Open(mysql.Open(conf.GetString("data.mysql.user")), &gorm.Config{Logger: logger})
	if err != nil {
		panic(err)
	}
	db = db.Debug()

	// db.AutoMigrate(&model.User{})

	db.Create(&model.User{
		UserId:    "123",
		Nickname:  "Test",
		Password:  "password",
		Email:     "test@example.com",
	
	})
	

	return nil
}
