package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type EmailverificationHandler struct {
	*Handler
	emailverificationService service.EmailverificationService
}

func NewEmailverificationHandler(handler *Handler, emailverificationService service.EmailverificationService) *EmailverificationHandler {
	return &EmailverificationHandler{
		Handler:                  handler,
		emailverificationService: emailverificationService,
	}
}

// Register godoc
//	@Summary	Email verification
//	@Schemes
//	@Description	Email verification
//	@Tags			Mailer
//	@Accept			json
//	@Produce		json
//	@Param			cid	path		string	true	"User_ID"
//	@Success		200	{object}	v1.Response
//	@Router			/emailverification/:cid [get]
func (h *EmailverificationHandler) SetEmailverification(ctx *gin.Context) {

	cid := ctx.Param("cid")

	if err := h.emailverificationService.ConfirmEmail(cid); err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	v1.HandleSuccess(ctx, nil)

}

// Register godoc
//	@Summary	Email verification By code
//	@Schemes
//	@Description	Email verification By code
//	@Tags			Mailer
//	@Accept			json
//	@Produce		json
//	@Param			request	body		v1.VerificationByCodeRequest	true	"params"
//	@Success		200		{object}	v1.Response
//	@Router			/verificationbycode [post]
func (h *EmailverificationHandler) VerificationByCode(ctx *gin.Context) {

	var req v1.VerificationByCodeRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	if err := h.emailverificationService.VerificationByCode(ctx, req.UserId, req.Code); err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	v1.HandleSuccess(ctx, nil)

}
