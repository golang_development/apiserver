package service

import (
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type PaymentService interface {
	GetPayment(id int64) (*model.Payment, error)
}

func NewPaymentService(service *Service, paymentRepository repository.PaymentRepository) PaymentService {
	return &paymentService{
		Service:        service,
		paymentRepository: paymentRepository,
	}
}

type paymentService struct {
	*Service
	paymentRepository repository.PaymentRepository
}

func (s *paymentService) GetPayment(id int64) (*model.Payment, error) {
	return s.paymentRepository.FirstById(id)
}
