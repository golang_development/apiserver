
##PRODUCTION ENV
k config use-context oracle
k create ns glass-go-prod
kubectl config set-context --current --namespace glass-go-prod

#MYSQL
kubectl apply -f deploy/K8S/production_env/mysql/mysql-pv.yaml
kubectl delete -f deploy/K8S/production_env/mysql/mysql-pv.yaml
kubectl get pv

kubectl apply -f deploy/K8S/production_env/mysql/configmap-init-sql.yaml
kubectl delete -f deploy/K8S/production_env/mysql/configmap-init-sql.yaml


kubectl apply -f deploy/K8S/production_env/mysql/mysql-deployment.yaml
kubectl delete -f deploy/K8S/production_env/mysql/mysql-deployment.yaml


#REDIS

helm repo update
helm install redis bitnami/redis -f deploy/K8S/production_env/redis/values.yaml
kubectl get secret --namespace glass-go-prod redis -o jsonpath="{.data.redis-password}" | base64 -d


#APISERVER

k config set-context --current --namespace glass-go-prod && make docker-prod && sleep 5 && k apply -f deploy/K8S/apiserver/apiserver-prod.yaml

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

