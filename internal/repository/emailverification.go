package repository

import (
	"context"
	"errors"

	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gorm.io/gorm"
)

type EmailverificationRepository interface {
	FirstById(id int64) error
	ConfirmEmail(cid string) error
	VerificationByCode(ctx context.Context, cid string, code int) error
}

func NewEmailverificationRepository(repository *Repository) EmailverificationRepository {
	return &emailverificationRepository{
		Repository: repository,
	}
}

type emailverificationRepository struct {
	*Repository
}

func (r *emailverificationRepository) FirstById(id int64) error {
	// var emailverification model.Emailverification
	// TODO: query db
	return nil
}

func (r *emailverificationRepository) ConfirmEmail(cid string) error {


	r.db.Model(&model.User{}).Where("user_id = ?", cid).Update("verify", true)

	return nil
}

func (r *emailverificationRepository) VerificationByCode(ctx context.Context, cid string, code int) error {
	var user model.User
	if err := r.DB(ctx).Where("user_id = ?", cid).First(&user).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil
		}
		return err
	}

	if user.Code == code {

		r.db.Model(&model.User{}).Where("user_id = ?", cid).Updates(model.User{Verify: true})

	} 

	return nil
}



