//go:build wireinject
// +build wireinject

package wire

import (
	"gitlab.com/glass-go/back/apiserver/internal/server"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
	"gitlab.com/glass-go/back/apiserver/pkg/app"
	"gitlab.com/glass-go/back/apiserver/pkg/log"
	"github.com/google/wire"
	"github.com/spf13/viper"
)

var jobSet = wire.NewSet(server.NewJob,repository.NewDB,repository.NewRedis,repository.NewRepository)

// build App
func newApp(job *server.Job) *app.App {
	return app.NewApp(
		app.WithServer(job),
		app.WithName("glsgo-job"),
	)
}

func NewWire(*viper.Viper, *log.Logger) (*app.App, func(), error) {
	panic(wire.Build(
		jobSet,
		newApp,
	))
}
