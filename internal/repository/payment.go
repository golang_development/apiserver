package repository

import (
	"gitlab.com/glass-go/back/apiserver/internal/model"
)

type PaymentRepository interface {
	FirstById(id int64) (*model.Payment, error)
}

func NewPaymentRepository(repository *Repository) PaymentRepository {
	return &paymentRepository{
		Repository: repository,
	}
}

type paymentRepository struct {
	*Repository
}

func (r *paymentRepository) FirstById(id int64) (*model.Payment, error) {
	var payment model.Payment
	// TODO: query db
	return &payment, nil
}
