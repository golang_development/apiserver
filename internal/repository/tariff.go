package repository

import (
	"context"
	"log"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"
)

type TariffRepository interface {
	FirstById(ctx context.Context,id int64) (*model.Tariff, error)
	GetTariffs(ctx context.Context) (*v1.GetTariffsResponce, error)
}

func NewTariffRepository(repository *Repository) TariffRepository {
	return &tariffRepository{
		Repository: repository,
	}
}

type tariffRepository struct {
	*Repository
}

func (r *tariffRepository) FirstById(ctx context.Context,id int64) (*model.Tariff, error) {
	var tariff model.Tariff

	log.Println("!!!!FirstById")
	// TODO: query db
	// r.DB(ctx).First(&tariff, 5)

	return &tariff, nil
}

func (r *tariffRepository) GetTariffs(ctx context.Context) (*v1.GetTariffsResponce, error) {
	

	var res v1.GetTariffsResponce
	var dbres []model.Tariff

	r.DB(ctx).Where("1 =1").Find(&dbres)

	var tfs []v1.TariffResponce

	for _, inv := range dbres {

		outinv := v1.TariffResponce{ID: inv.ID, Description: inv.Description, Cost: inv.Cost, Duration: inv.Duration, Clicklimit: inv.Clicklimit}
		tfs = append(tfs, outinv)

	}

	res.Tariffs = tfs

	return &res, nil
}
