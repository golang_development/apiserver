package service

import (
	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type ChattableService interface {
	GetChattable(limit int) ([]model.Chattable, error)
	InsertChattable(res v1.ChattableInsert) (v1.ChatResponseData, error)
}

func NewChattableService(service *Service, chattableRepository repository.ChattableRepository) ChattableService {
	return &chattableService{
		Service:             service,
		chattableRepository: chattableRepository,
	}
}

type chattableService struct {
	*Service
	chattableRepository repository.ChattableRepository
}

func (s *chattableService) GetChattable(limit int) ([]model.Chattable, error) {
	return s.chattableRepository.GetAll(limit)
}
func (s *chattableService) InsertChattable(res v1.ChattableInsert) (v1.ChatResponseData, error) {
	// Generate user ID
	var ret v1.ChatResponseData
	recId, err := s.sid.GenString()
	if err != nil {
		return ret, err
	}

	rec := &model.Chattable{
		RecId:   recId,
		UserId:  res.UserId,
		Mlmodel: res.Mlmodel,
		Ask:     res.Ask,
	}

	ret, err = s.chattableRepository.Insert(rec)
	if err != nil {
		return ret, err
	}

	return ret, nil
}
