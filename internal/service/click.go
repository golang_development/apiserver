package service

import (
	"context"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type ClickService interface {
	GetClick(ctx context.Context,cid string) (*v1.ClicksResponce, error)
}

func NewClickService(service *Service, clickRepository repository.ClickRepository) ClickService {
	return &clickService{
		Service:        service,
		clickRepository: clickRepository,
	}
}

type clickService struct {
	*Service
	clickRepository repository.ClickRepository
}

func (s *clickService) GetClick(ctx context.Context,cid string) (*v1.ClicksResponce, error) {
	return s.clickRepository.FirstById(ctx,cid)
}
