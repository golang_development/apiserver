package itest_test

import (
	"fmt"
	"io"
	"log"
	"net/http"
)

func subClicks(s *e2eTestSuite) {
	request, err := http.NewRequest("GET", fmt.Sprintf("%s/v1/clicks", s.ts.URL), nil)
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.bearer)
	if err != nil {

		s.NoError(err)

	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err := io.ReadAll(response.Body)
	if err != nil {
		s.NoError(err)

	}

	log.Println(string(sbyteBody))

	s.Equal(200, response.StatusCode)


	s.Run("subAdminapi", func() {

		subAdminapi(s)

	})

}
