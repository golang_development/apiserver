package repository

import (
	"context"
	"log"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"
)

type ClickRepository interface {
	FirstById(ctx context.Context, cid string) (*v1.ClicksResponce, error)
}

func NewClickRepository(repository *Repository) ClickRepository {
	return &clickRepository{
		Repository: repository,
	}
}

type clickRepository struct {
	*Repository
}

func (r *clickRepository) FirstById(ctx context.Context, cid string) (*v1.ClicksResponce, error) {
	var click v1.ClicksResponce

	var user model.User
	r.DB(ctx).Where("user_id =?", cid).Find(&user)

	numclicks := user.Clicks

	log.Println("!!!CLICKS", numclicks)

	if numclicks > 0 {
		clicksleft := numclicks -1
		log.Println("!!!CLICKS SACE", clicksleft)

		// user.Clicks = clicksleft

		// r.DB(ctx).Save(&user)

		r.db.Model(&model.User{}).Where("user_id = ?", cid).Update("clicks",clicksleft)

		// clicksleft := numclicks -1

		// r.DB(ctx).Where("user_id =?", cid).Update("clicks = ?",clicksleft)

		click.Clicksleft = numclicks - 1

	}

	return &click, nil
}
