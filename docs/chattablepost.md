# Get answer from ML service and insert result into DB


## DB table used
`
CREATE TABLE `chattable` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `deleted_at` datetime(3) DEFAULT NULL,
  `rec_id` varchar(191) NOT NULL,
  `user_id` longtext NOT NULL,
  `mlmodel` longtext NOT NULL,
  `ask` longtext NOT NULL,
  `answer` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rec_id` (`rec_id`),
  KEY `idx_chattable_deleted_at` (`deleted_at`)
)
`

