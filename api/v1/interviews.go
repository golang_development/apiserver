package v1


type InsertInterviewResultRequest struct {
	Item string `json:"item" example:"frontend"`
	InterviewJsonData string `json:"interviewjsondata" example:"[{\"answer\":\"text\",\"correct\":false},{\"answer\":\"text\",\"correct\":true}]"` 

}