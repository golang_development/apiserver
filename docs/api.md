# General API documentation

Go server to manipulate differen ML models

it will act as a proxy engine and will be served ideally behind an HTTP cache system like **memcache,varnish,cloudflare**.  

The following ML models are supported:

* ChatGpt
* Yandex (not yet implmented!)
* SBER (not yet implmented!)
* etc .......

### Examples:
`curl https://api.glass-go.rest/v1/ask -H 'Content-Type: application/json' -d '{"clid":"lslsl8282","ask":"What do you think about Finland"}'`

## Result:

`{"code":0,"message":"ok","data":{"answer":"I think Finland is a beautiful country with a rich culture and history. It is known for its stunning landscapes..."}}`


## Interview

`curl  https://api.glass-go.rest/v1/insertinterviewsresult -H 'Content-Type: application/json'  -H 'Authorization: Bearer eyJhb...'  -d '[{"answer":"text","correct":false},{"answer":"text","correct":true}]'`

`curl -X 'GET'  -H 'Content-Type: application/json' 'https://api.glass-go.rest/v1/interviewsskill/AEz26UeaxJ' -H 'Authorization: Bearer eyJh...`


## Users table

CREATE TABLE `users` (
  `user_id` varchar(191) NOT NULL,
  `nickname` longtext NOT NULL,
  `password` longtext NOT NULL,
  `email` longtext NOT NULL,
  `role` varchar(191) DEFAULT 'tester',
  `balance` double DEFAULT '0',
  `block` tinyint(1) DEFAULT '0',
  `verify` tinyint(1) DEFAULT '0',
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  UNIQUE KEY `user_id` (`user_id`)
)



