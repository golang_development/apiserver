package pushjob

import (
	"context"
	"encoding/json"
	"log/slog"
	"os"
	"os/signal"

	"github.com/kalbhor/tasqueue/v2"
	"gitlab.com/glass-go/back/apiserver/internal/model"

	"gitlab.com/glass-go/back/apiserver/pkg/tasks"
)

func PushIntoQue(rec *model.Chattable, srv model.TaskQue) error {
	ctx, _ := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	lo := slog.Default()

	chattable := model.Chattable{RecId: rec.RecId, UserId: rec.UserId, Mlmodel: rec.Mlmodel, Ask: rec.Ask, Answer: rec.Answer}

	payload := tasks.Payload{Sqlurl: srv.Sqlurls, Chattabale: chattable}

	b, _ := json.Marshal(payload)
	job, err := tasqueue.NewJob("insertdb", b, tasqueue.JobOpts{})
	if err != nil {
		return err
	}

	id, err := srv.Tasqueue.Enqueue(ctx, job)
	if err != nil {
		return err
	}

	lo.Info("id", "ID", id)


	return nil

	// jobMsg, err := srv.Tasqueue.GetJob(ctx, id)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// lo.Info("msh", "MSG", jobMsg)

	// time.Sleep(time.Second)

	// jobMsg, err = srv.Tasqueue.GetJob(ctx, id)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// lo.Info("msh2", "MSG2", jobMsg)

	// time.Sleep(time.Second*2)

	// jobMsg, err = srv.Tasqueue.GetJob(ctx, id)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// lo.Info("msh2", "MSG3", jobMsg)

	// time.Sleep(time.Second*2)

	// b, err = srv.Tasqueue.GetResult(ctx, id)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// lo.Info("RESULT", "MSG4", b)

	// rec := model.Chattable{Ask: "ask",Answer: "answer"}

	// result := db.Create(&rec)
	// if result.Error != nil {
	// 	return
	// }

}
