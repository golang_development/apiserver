package main

import (
	"context"
	"flag"
	"gitlab.com/glass-go/back/apiserver/cmd/server/wire"
	"gitlab.com/glass-go/back/apiserver/pkg/config"
	"gitlab.com/glass-go/back/apiserver/pkg/log"
	"go.uber.org/zap"
)

//	@title		GlassGo
//	@version	0.0.0
//	@description.markdown
//	@termsOfService				http://swagger.io/terms/
//	@contact.name				API Support
//	@contact.url				http://www.swagger.io/support
//	@contact.email				support@glass-go.rest
//	@license.name				Apache 2.0
//	@license.url				http://www.apache.org/licenses/LICENSE-2.0.html
//	@host						api.glass-go.rest
//	@basePath					/v1
//	@schemes					https
//	@securityDefinitions.apiKey	Bearer
//	@in							header
//	@name						Authorization
//	@description				Type "Bearer" followed by a space and JWT token.
//	@externalDocs.description	OpenAPI
//	@externalDocs.url			https://swagger.io/resources/open-api/
func main() {
	var envConf = flag.String("conf", "config/local.yml", "config path, eg: -conf ./config/local.yml")
	flag.Parse()
	conf := config.NewConfig(*envConf)

	logger := log.NewLog(conf)

	app, cleanup, err := wire.NewWire(conf, logger)
	defer cleanup()
	if err != nil {
		panic(err)
	}
	logger.Info("server start", zap.String("host", "http://127.0.0.1:"+conf.GetString("http.port")))
	if err = app.Run(context.Background()); err != nil {
		panic(err)
	}
}
