package service

import (
	"context"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type AdminService interface {
	GetUserById(ctx context.Context,cid string) (*v1.GetUserResponseData, error)
	GetAllUsers(ctx context.Context) (*v1.GetAllUsersResponce, error)
	GetAllClickstat(ctx context.Context) (*v1.GetClickstatResponce, error)
	GetClickstatById(ctx context.Context, cid string) (*v1.GetClickstatResponce, error)

}

func NewAdminService(service *Service, adminRepository repository.AdminRepository) AdminService {
	return &adminService{
		Service:        service,
		adminRepository: adminRepository,
	}
}

type adminService struct {
	*Service
	adminRepository repository.AdminRepository
}

func (s *adminService) GetUserById(ctx context.Context,cid string) (*v1.GetUserResponseData, error) {

	var res v1.GetUserResponseData

	user,err :=s.adminRepository.FirstById(ctx,cid)
	if err !=nil {
		return &res,err
	}

	res.Email = user.Email
	res.Verify = user.Verify
	res.Nickname = user.Nickname
	res.Tariff = uint(user.Tariff)
	res.Clicks = user.Clicks
	res.Interview = user.Interview
	res.InterviewPermission = user.InterviewPermission
	res.Block = user.Block
	res.DurationTill = user.DurationTill
	res.CreatedAt = user.CreatedAt
	res.UpdatedAt = user.UpdatedAt


	return &res,nil
}


func (s *adminService) GetAllUsers(ctx context.Context) (*v1.GetAllUsersResponce, error) {
	return s.adminRepository.GetAllUsers(ctx)
}


func (s *adminService) GetAllClickstat(ctx context.Context) (*v1.GetClickstatResponce, error) {

	return s.adminRepository.GetAllClickstat(ctx)

}

func (s *adminService) GetClickstatById(ctx context.Context, cid string) (*v1.GetClickstatResponce, error) {

	return s.adminRepository.GetClickstatById(ctx,cid)

}
