package service

import (
	"context"

	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type EmailverificationService interface {
	GetEmailverification(id int64) error
	ConfirmEmail(cid string) error
	VerificationByCode(ctx context.Context, cid string, code int) error
}

func NewEmailverificationService(service *Service, emailverificationRepository repository.EmailverificationRepository) EmailverificationService {
	return &emailverificationService{
		Service:        service,
		emailverificationRepository: emailverificationRepository,
	}
}

type emailverificationService struct {
	*Service
	emailverificationRepository repository.EmailverificationRepository
}

func (s *emailverificationService) GetEmailverification(id int64) error {
	return s.emailverificationRepository.FirstById(id)
}
func (s *emailverificationService) ConfirmEmail(cid string) error {
	return s.emailverificationRepository.ConfirmEmail(cid)
}

func (s *emailverificationService) VerificationByCode(ctx context.Context,cid string,code int) error {
	return s.emailverificationRepository.VerificationByCode(ctx,cid,code)
}