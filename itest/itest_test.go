package itest_test

import (
	"flag"
	"fmt"
	"os"

	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/suite"
	"gitlab.com/glass-go/back/apiserver/internal/handler"
	"gitlab.com/glass-go/back/apiserver/internal/middleware"
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
	"gitlab.com/glass-go/back/apiserver/internal/service"
	"gitlab.com/glass-go/back/apiserver/pkg/config"
	"gitlab.com/glass-go/back/apiserver/pkg/helper/sid"
	"gitlab.com/glass-go/back/apiserver/pkg/jwt"

	"gitlab.com/glass-go/back/apiserver/pkg/log"

	apiV1 "gitlab.com/glass-go/back/apiserver/api/v1"

	"gorm.io/gorm"
)

type e2eTestSuite struct {
	suite.Suite
	ts          *httptest.Server
	db          *gorm.DB
	conf        *viper.Viper
	bearer      string
	cid         string
	code        float64
	email       string
	shopid      string
	shoppass1   string
	shoppass2   string
	adminbearer string
	// role   string
}

func TestE2ETestSuite(t *testing.T) {
	suite.Run(t, &e2eTestSuite{})
}

func (s *e2eTestSuite) SetupSuite() {

	// err := os.Setenv("APP_CONF", "../config/local.yml")
	err := os.Setenv("APP_CONF", "../config/local.yml")
	if err != nil {
		fmt.Println("Setenv error", err)
	}
	var envConf = flag.String("conf", "config/local.yml", "config path, eg: -conf ./config/local.yml")
	flag.Parse()
	s.conf = config.NewConfig(*envConf)

	logger := log.NewLog(s.conf)

	s.shopid = "glsgo"
	s.shoppass1 = "QXoiiERO76a342LlbQvb"
	s.shoppass2 = "TkkeZOH44vkqXo7PY13w"

	robo := repository.NewRobo(s.conf)
	client := repository.NewRedis(s.conf)
	restyClient := repository.NewResty(s.conf)
	sidSid := sid.NewSid()
	jwtJWT := jwt.NewJwt(s.conf)
	tasqueue := repository.NewTasqueue(s.conf)

	handlerHandler := handler.NewHandler(logger)
	s.db = repository.NewDB(s.conf, logger)
	repositoryRepository := repository.NewRepository(s.db, client, restyClient, logger, robo, tasqueue)
	transaction := repository.NewTransaction(repositoryRepository)
	serviceService := service.NewService(transaction, logger, sidSid, jwtJWT)

	userRepository := repository.NewUserRepository(repositoryRepository)
	userService := service.NewUserService(serviceService, userRepository)
	userHandler := handler.NewUserHandler(handlerHandler, userService)

	chattableRepository := repository.NewChattableRepository(repositoryRepository)
	chattableService := service.NewChattableService(serviceService, chattableRepository)
	chattableHandler := handler.NewChattableHandler(handlerHandler, chattableService)

	creditcardRepository := repository.NewCreditcardRepository(repositoryRepository)
	creditcardService := service.NewCreditcardService(serviceService, creditcardRepository)
	creditcardHandler := handler.NewCreditcardHandler(handlerHandler, creditcardService)

	emailverificationRepository := repository.NewEmailverificationRepository(repositoryRepository)
	emailverificationService := service.NewEmailverificationService(serviceService, emailverificationRepository)
	emailverificationHandler := handler.NewEmailverificationHandler(handlerHandler, emailverificationService)

	paymentRepository := repository.NewPaymentRepository(repositoryRepository)
	paymentService := service.NewPaymentService(serviceService, paymentRepository)
	paymentHandler := handler.NewPaymentHandler(handlerHandler, paymentService)

	paymenthookRepository := repository.NewPaymenthookRepository(repositoryRepository)
	paymenthookService := service.NewPaymenthookService(serviceService, paymenthookRepository)
	paymenthookHandler := handler.NewPaymenthookHandler(handlerHandler, paymenthookService)

	paymenturlRepository := repository.NewPaymenturlRepository(repositoryRepository)
	paymenturlService := service.NewPaymenturlService(serviceService, paymenturlRepository)
	paymenturlHandler := handler.NewPaymenturlHandler(handlerHandler, paymenturlService)

	interviewskillRepository := repository.NewInterviewskillRepository(repositoryRepository)
	interviewskillService := service.NewInterviewskillService(serviceService, interviewskillRepository)
	interviewskillHandler := handler.NewInterviewskillHandler(handlerHandler, interviewskillService)

	tariffRepository := repository.NewTariffRepository(repositoryRepository)
	tariffService := service.NewTariffService(serviceService, tariffRepository)
	tariffHandler := handler.NewTariffHandler(handlerHandler, tariffService)

	clickRepository := repository.NewClickRepository(repositoryRepository)
	clickService := service.NewClickService(serviceService, clickRepository)
	clickHandler := handler.NewClickHandler(handlerHandler, clickService)

	adminRepository := repository.NewAdminRepository(repositoryRepository)
	adminService := service.NewAdminService(serviceService, adminRepository)
	adminHandler := handler.NewAdminHandler(handlerHandler, adminService)

	s.db.Migrator().DropTable(&model.User{})
	s.db.Migrator().DropTable(&model.Chattable{})
	// s.db.Migrator().DropTable(&model.Payment{})
	// s.db.Migrator().DropTable(&model.Creditcard{})
	s.db.Migrator().DropTable(&model.Invoice{})
	s.db.Migrator().DropTable(&model.Interviewskill{})
	s.db.Migrator().DropTable(&model.Tariff{})
	s.db.Migrator().DropTable(&model.Clickstat{})

	s.db.Migrator().CreateTable(&model.User{})
	s.db.Migrator().CreateTable(&model.Chattable{})
	// s.db.Migrator().CreateTable(&model.Payment{})
	s.db.Migrator().CreateTable(&model.Invoice{})
	// s.db.Migrator().CreateTable(&model.Creditcard{})
	// s.db.Migrator().CreateTable(&model.Invite{})
	s.db.Migrator().CreateTable(&model.Interviewskill{})
	s.db.Migrator().CreateTable(&model.Tariff{})
	s.db.Migrator().CreateTable(&model.Clickstat{})

	s.db.Model(&model.Invite{}).Where("1 = 1").Update("block", false)

	var tariffs = []model.Tariff{{Description: "1 месяц", Cost: 199, Duration: 1, Clicklimit: 70}, {Description: "3 месяца", Cost: 500, Duration: 3, Clicklimit: 70},
		{Description: "6 месяц", Cost: 900, Duration: 6, Clicklimit: 70}, {Description: "1 rub", Cost: 1, Duration: 0, Clicklimit: 20}, {Description: "Бесплатный", Cost: 0, Duration: 0, Clicklimit: 20}}

	s.db.Create(tariffs)

	// var invites = []model.Invite{{Invcode: "alex", Item: "frontend"}, {Invcode: "vlad", Item: "frontend"}, {Invcode: "alexander", Item: "frontend"}, {Invcode: "slava", Item: "frontend"}, {Invcode: "roma", Item: "frontend"},
	// 	{Invcode: "invite0", Item: "frontend"}, {Invcode: "invite1", Item: "frontend"}, {Invcode: "invite2", Item: "frontend"}, {Invcode: "invite3", Item: "frontend"}, {Invcode: "invite4", Item: "frontend"}, {Invcode: "invite5", Item: "frontend"},
	// 	{Invcode: "invite6", Item: "frontend"}, {Invcode: "invite7", Item: "frontend"}, {Invcode: "invite8", Item: "frontend"}, {Invcode: "invite9", Item: "frontend"}, {Invcode: "invite10", Item: "frontend"}}

	// for _, inv := range invites {

	// 	result := s.db.Create(&inv)

	// 	s.NoError(result.Error)

	// }

	// for i := 0; i < 10000; i++ {
	// 	gennewpass := generatepassword.GeneratePassword(12, true, true, true)

	// 	inv := model.Invite{Invcode: gennewpass, Item: "frontend"}

	// 	result := s.db.Create(&inv)

	// 	s.NoError(result.Error)

	// }

	gin.SetMode(gin.TestMode)
	router := gin.Default()
	router.Use(
		middleware.CORSMiddleware(),
		middleware.ResponseLogMiddleware(logger),
		middleware.RequestLogMiddleware(logger),
		//middleware.SignMiddleware(log),
	)

	router.GET("/", func(ctx *gin.Context) {
		logger.WithContext(ctx).Info("hello")
		apiV1.HandleSuccess(ctx, map[string]interface{}{
			":)": "Thank you for using Glass-go apiserver!",
		})
	})

	// router := server.Start()
	v1 := router.Group("/v1")
	{
		noStrictAuthRouterAdmin := v1.Group("/admin").Use(middleware.NoStrictAuth(jwtJWT, logger))
		{
			noStrictAuthRouterAdmin.GET("/allusers", adminHandler.GetAllUsers)
			noStrictAuthRouterAdmin.GET("/user", adminHandler.GetUserById)
		}
		noAuthRouter := v1.Group("/")
		{
			noAuthRouter.POST("/register", userHandler.Register)
			noAuthRouter.POST("/login", userHandler.Login)
			noAuthRouter.POST("/chattableinternal", chattableHandler.InsertChattable)
			noAuthRouter.GET("/payment", paymentHandler.GetPayment)
			noAuthRouter.GET("/creditcard", creditcardHandler.GetCreditcard)
			noAuthRouter.GET("/emailverification/:cid", emailverificationHandler.SetEmailverification)
			noAuthRouter.POST("/verificationbycode", emailverificationHandler.VerificationByCode)
			noAuthRouter.POST("/forgotpassword", userHandler.ForgotPassword)
			noAuthRouter.GET("/paymenthook", paymenthookHandler.GetPaymenthook)
			noAuthRouter.GET("/notpaid", paymenthookHandler.NotPaid)
		}

		// Non-strict permission routing group
		noStrictAuthRouter := v1.Group("/").Use(middleware.NoStrictAuth(jwtJWT, logger))
		{
			noStrictAuthRouter.GET("/user", userHandler.GetProfile)

			// #Tariffs
			noStrictAuthRouter.GET("/tariffs", tariffHandler.GetTariffs)

			// #Clicks
			noStrictAuthRouter.GET("/clicks", clickHandler.GetClick)

		}

		// Strict permission routing group
		strictAuthRouter := v1.Group("/").Use(middleware.StrictAuth(jwtJWT, logger))
		{
			// #User
			strictAuthRouter.PUT("/user", userHandler.UpdateProfile)
			strictAuthRouter.POST("/chattable", chattableHandler.InsertChattable)
			strictAuthRouter.GET("/chattable", chattableHandler.GetChattable)
			strictAuthRouter.POST("/updatepassword", userHandler.UpdatePassword)
			strictAuthRouter.DELETE("/block/:cid", userHandler.BlockProfile)

			// #Payments
			strictAuthRouter.POST("/paymenturl", paymenturlHandler.GetPaymenturl)
			strictAuthRouter.GET("/getinvoicesbyclid", paymenturlHandler.GetInvoicesByClid)

			// #Interview
			strictAuthRouter.GET("/interviewsskill/:id", interviewskillHandler.GetInterviewskill)
			strictAuthRouter.POST("/insertinterviewsresult", interviewskillHandler.InserInterviewsresult)
		}

	}
	s.ts = httptest.NewServer(router)

}

func (s *e2eTestSuite) TearDownTest() {

	// s.db.Exec("DELETE FROM users")

	// s.db.Migrator().DropTable("users")
	// s.db.Migrator().DropTable("chattable")

}

func (s *e2eTestSuite) Test_admin() {

	s.Run("subTestAdmin", func() {

		Adminuser(s)

	})

}
