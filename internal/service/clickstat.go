package service

import (
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type ClickstatService interface {
	GetClickstat(id int64) (*model.Clickstat, error)
	// GetAllClickstat(ctx context.Context) (*v1.GetClickstatResponce, error)
}

func NewClickstatService(service *Service, clickstatRepository repository.ClickstatRepository) ClickstatService {
	return &clickstatService{
		Service:             service,
		clickstatRepository: clickstatRepository,
	}
}

type clickstatService struct {
	*Service
	clickstatRepository repository.ClickstatRepository
}

func (s *clickstatService) GetClickstat(id int64) (*model.Clickstat, error) {
	return s.clickstatRepository.FirstById(id)
}

// func (s *clickstatService) GetAllClickstat(ctx context.Context) (*v1.GetClickstatResponce, error) {

// 	return s.clickstatRepository.GetAllClickstat(ctx)

// }
