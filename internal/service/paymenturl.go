package service

import (
	"context"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"

	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type PaymenturlService interface {
	GetInvoicesByClid(ctx context.Context, userid string) (v1.GetInvoicesByClidResponce, error)
	GetPaymenturl(ctx context.Context,userid string,req v1.GetPaymenturlRequest) (string, error)

}

func NewPaymenturlService(service *Service, paymenturlRepository repository.PaymenturlRepository) PaymenturlService {
	return &paymenturlService{
		Service:        service,
		paymenturlRepository: paymenturlRepository,
	}
}

type paymenturlService struct {
	*Service
	paymenturlRepository repository.PaymenturlRepository
}

func (s *paymenturlService) GetInvoicesByClid(ctx context.Context,userid string) (v1.GetInvoicesByClidResponce, error) {
	return s.paymenturlRepository.GetInvoicesByClid(ctx,userid)
}


func (s *paymenturlService) GetPaymenturl(ctx context.Context,userid string,req v1.GetPaymenturlRequest) (string, error) {
	return s.paymenturlRepository.GetPaymentUrl(ctx,userid,req)
}
