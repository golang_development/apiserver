package v1

type ChatgptRequest struct {
	Clid string `json:"clid" example:"clid919191"`
	Ask string `json:"ask" example:"What do you think about Finland?"`
}

type ChatgptResponseData struct {
	Answer string `json:"answer" example:"Finland good for old man!"` 
}
