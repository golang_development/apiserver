package chatgpt

import (
	"encoding/json"
	"github.com/go-resty/resty/v2"
)

type Answer struct {
	Choices []struct {
		FinishReason string `json:"finish_reason"`
		Index        int    `json:"index"`
		Message      struct {
			Content string `json:"content"`
			Role    string `json:"role"`
		} `json:"message"`
	} `json:"choices"`
	Created           int         `json:"created"`
	ID                string      `json:"id"`
	Model             string      `json:"model"`
	Object            string      `json:"object"`
	SystemFingerprint interface{} `json:"system_fingerprint"`
	Usage             struct {
		CompletionTokens int `json:"completion_tokens"`
		PromptTokens     int `json:"prompt_tokens"`
		TotalTokens      int `json:"total_tokens"`
	} `json:"usage"`
}

func Ask(client *resty.Client, apiEndpoint string, ask string) (string, error) {

	response, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(map[string]interface{}{
			// "model":      "gpt-3.5-turbo",
			"model":      "gpt-4-1106-preview",
			"messages":   []interface{}{map[string]interface{}{"role": "system", "content": ask}},
			"max_tokens": 1000,
		}).
		Post(apiEndpoint)

	if err != nil {

		return "", err
	}

	body := response.Body()

	var data Answer
	err = json.Unmarshal(body, &data)
	if err != nil {
		return "", err
	}

	res := data.Choices[0].Message.Content

	return res, nil
}
