package rangeint

import "math/rand"

func RangeIn(low, hi int) int {
	return low + rand.Intn(hi-low)
}

// type numRange struct {
// 	low int
// 	hi  int
// }