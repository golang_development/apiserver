package sendemail

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)
type ForgetPasswordEmailMod struct {
	Email string `json:"email" example:"nordeasipvip@gmail.com"`
	NewPassword string `json:"newpassword" example:"kdkaadksks1121221"`
}
func SendEmail(url string, email string, content string) error {

	emailout := ForgetPasswordEmailMod{Email: email,NewPassword: content}

	b, err := json.Marshal(emailout)
	if err != nil {
		return err
	}


	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer([]byte(b)))
	if err != nil {
		fmt.Printf("client: could not create request: %s\n", err)
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	client := http.Client{
		Timeout: 30 * time.Second,
	}

	_, err = client.Do(req)
	if err != nil {
		fmt.Printf("client: error making http request: %s\n", err)
		return err
	}

	return nil
}
