package tasks

import (
	"encoding/json"
	"log"

	"github.com/kalbhor/tasqueue/v2"
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)


type Payload struct {

	Sqlurl string `json:"sqlurl"`
	Chattabale model.Chattable`json:"chattabale"`
	
}


// SumProcessor prints the sum of two integer arguements.
func InsertDBProcessor(b []byte, m tasqueue.JobCtx) error {

	var pl Payload
	if err := json.Unmarshal(b, &pl); err != nil {
		return err
	}

	db, err := gorm.Open(mysql.Open(pl.Sqlurl), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	db = db.Debug()

	rec := model.Chattable{RecId: pl.Chattabale.RecId,UserId: pl.Chattabale.UserId,  Mlmodel: pl.Chattabale.Mlmodel, Ask: pl.Chattabale.Ask, Answer: pl.Chattabale.Answer}

	result := db.Create(&rec)
	if result.Error != nil {
		log.Fatal(err)
	
	}

	err = m.Save(b)
	if err != nil {
		return err
	}

	sqlDB, err := db.DB()
	sqlDB.Close()

	return nil
}

// func MlProcessor(b []byte, m tasqueue.JobCtx) error {
// 	var ask models.ChattableInsert
// 	if err := json.Unmarshal(b, &ask); err != nil {
// 		return err
// 	}

// 	answer, err := askml.Ask("https://api.glass-go.rest", ask)
// 	if err != nil {
// 		return err
// 	}

// 	rs, err := json.Marshal(answer)
// 	if err != nil {
// 		return err
// 	}

// 	log.Println(answer)

// 	m.Save(rs)

// 	return nil
// }
