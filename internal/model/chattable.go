package model

import (
	"time"

	"github.com/kalbhor/tasqueue/v2"
)

type Chattable struct {
	ID        uint   `gorm:"primaryKey"`
	RecId     string `gorm:"unique;not null"`
	UserId    string `gorm:"not null"`
	Mlmodel   string `gorm:"not null"`
	Ask       string `gorm:"not null"`
	Answer    string `gorm:"not null"`
	CreatedAt time.Time
	UpdatedAt time.Time
	
}

func (m *Chattable) TableName() string {
	return "chattable"
}
type TaskQue  struct {
	Sqlurls string
	Tasqueue *tasqueue.Server

}