package server

import (
	"context"
	"time"

	"github.com/go-co-op/gocron"

	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/pkg/log"
	"go.uber.org/zap"

	"gorm.io/gorm"
)

type Task struct {
	log       *log.Logger
	scheduler *gocron.Scheduler
	db        *gorm.DB
}

func NewTask(db *gorm.DB, log *log.Logger) *Task {
	return &Task{
		db:  db,
		log: log,
	}
}
func (t *Task) Start(ctx context.Context) error {
	gocron.SetPanicHandler(func(jobName string, recoverData interface{}) {
		t.log.Error("Task Panic", zap.String("job", jobName), zap.Any("recover", recoverData))
	})

	// t.db = NewDB()

	// eg: crontab task
	t.scheduler = gocron.NewScheduler(time.UTC)
	// if you are in China, you will need to change the time zone as follows
	// t.scheduler = gocron.NewScheduler(time.FixedZone("PRC", 8*60*60))

	maptariffs := make(map[int]int)

	// 0 21
	_, err := t.scheduler.Cron("0 21 * * *").Do(func() {
		// _, err := t.scheduler.Cron("*/2 * * * *").Do(func() {
		t.log.Info("Reset clicks.")

		var tariffs []model.Tariff
		result := t.db.Find(&tariffs)

		t.log.Info("tarifs num.", zap.Int64("row", result.RowsAffected))

		for _, trf := range tariffs {
			maptariffs[int(trf.ID)] = trf.Clicklimit

		}

		var users []model.User

		t.db.Where("clicks > 0 ").Find(&users)

		for _, user := range users {

			clickLm := maptariffs[int(user.Tariff)]

			if user.Clicks > 0 && user.Clicks < clickLm {

				clickstat := model.Clickstat{UserId: user.UserId, Clicks: user.Clicks, ClicksLimit: clickLm}

				t.db.Save(&clickstat)

				user.Clicks = clickLm

				t.db.Model(&model.User{}).Where("user_id=?", user.UserId).Update("clicks", clickLm)

			}

		}

	})
	if err != nil {
		t.log.Error("Task1 error", zap.Error(err))
	}


	t.scheduler.StartBlocking()
	return nil
}
func (t *Task) Stop(ctx context.Context) error {
	t.scheduler.Stop()
	t.log.Info("Task stop...")
	return nil
}

