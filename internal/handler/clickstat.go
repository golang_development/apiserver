package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type ClickstatHandler struct {
	*Handler
	clickstatService service.ClickstatService
}

func NewClickstatHandler(handler *Handler, clickstatService service.ClickstatService) *ClickstatHandler {
	return &ClickstatHandler{
		Handler:      handler,
		clickstatService: clickstatService,
	}
}

func (h *ClickstatHandler) GetClickstat(ctx *gin.Context) {

}
