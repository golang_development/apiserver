package model

import "gorm.io/gorm"

type Tariff struct {
	gorm.Model
	Description string
	Cost int
	Duration int
	Clicklimit int
}

func (m *Tariff) TableName() string {
    return "tariffs"
}
