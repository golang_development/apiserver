package v1

type GetTariffsResponce struct {
	Tariffs []TariffResponce `json:"tariffs" `
}

type TariffResponce struct {
	ID          uint   `json:"id" example:"1"`
	Description string `json:"description" example:"1 месяц"`
	Cost        int    `json:"cost" example:"1"`
	Duration    int    `json:"duration" example:"2"`
	Clicklimit  int    `json:"clicklimit" example:"70"`
}
