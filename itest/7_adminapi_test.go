package itest_test

import (

	"fmt"
	"io"
	"log"
	"net/http"

)

func subAdminapi(s *e2eTestSuite) {


	// jsonData := &v1.LoginRequest{
	// 	Password: "glassFAN01",
	// 	Email:    "admin",
		
	// }

	// b, err := json.Marshal(jsonData)
	// if err != nil {
	// 	s.NoError(err)
	// }

	// request, err := http.NewRequest("POST", fmt.Sprintf("%s/v1/login", s.ts.URL), bytes.NewBuffer(b))
	// request.Header.Set("Content-Type", "application/json;")
	// if err != nil {

	// 	s.NoError(err)

	// }

	// response, err := http.DefaultClient.Do(request)
	// if err != nil {
	// 	s.NoError(err)

	// }

	// sbyteBody, err := io.ReadAll(response.Body)

	// if err != nil {

	// 	s.NoError(err)

	// }
	// var resp v1.Response

	// log.Println("!!!RSPONCE",string(sbyteBody))

	// err = json.Unmarshal(sbyteBody, &resp)
	// if err != nil {

	// 	s.NoError(err)

	// }
	// myMap := resp.Data.(map[string]interface{})

	// log.Println("!!!MAP",myMap)

	// s.adminbearer = myMap["accessToken"].(string)

	// defer response.Body.Close()

	// s.Equal(200, response.StatusCode)



	// log.Println("!!!JWTnordea", s.bearer)
	// log.Println("!!!JWTADMIN", s.adminbearer)


	request, err := http.NewRequest("GET", fmt.Sprintf("%s/v1/admin/allusers", s.ts.URL), nil)
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.adminbearer)
	if err != nil {

		s.NoError(err)

	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err:= io.ReadAll(response.Body)
	if err != nil {
		s.NoError(err)

	}

	log.Println(string(sbyteBody))

	s.Equal(200, response.StatusCode)

	request, err = http.NewRequest("GET", fmt.Sprintf("%s/v1/admin/user?cid="+s.cid, s.ts.URL), nil)
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.bearer)
	if err != nil {

		s.NoError(err)

	}

	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err = io.ReadAll(response.Body)
	if err != nil {
		s.NoError(err)

	}

	log.Println(string(sbyteBody))

	s.Equal(200, response.StatusCode)

	////BLOCK user
	request, err = http.NewRequest("DELETE", fmt.Sprintf("%s/v1/block/"+s.cid+"?block=1", s.ts.URL), nil)
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.bearer)
	if err != nil {

		s.NoError(err)

	}

	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err = io.ReadAll(response.Body)
	if err != nil {
		s.NoError(err)

	}

	log.Println(string(sbyteBody))

	s.Equal(200, response.StatusCode)

	//UNBLOCK user
	request, err = http.NewRequest("DELETE", fmt.Sprintf("%s/v1/block/"+s.cid+"?block=0", s.ts.URL), nil)
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.bearer)
	if err != nil {

		s.NoError(err)

	}

	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err = io.ReadAll(response.Body)
	if err != nil {
		s.NoError(err)

	}

	log.Println(string(sbyteBody))

	s.Equal(200, response.StatusCode)


}
