package itest_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	
	"net/http"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"
)

func Adminuser(s *e2eTestSuite) {

	jsonData := &v1.RegisterRequest{
		Password: "glassFAN01",
		Email:    "admin",
		Nickname: "admin",
	}

	b, err := json.Marshal(jsonData)
	if err != nil {
		s.NoError(err)
	}

	request, err := http.NewRequest("POST", fmt.Sprintf("%s/v1/register", s.ts.URL), bytes.NewBuffer(b))
	request.Header.Set("Content-Type", "application/json;")
	if err != nil {

		s.NoError(err)

	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	defer response.Body.Close()

	s.Equal(200, response.StatusCode)

	//// SUPPORT USER
	jsonDataSup := &v1.RegisterRequest{
		Password: "glassSUP02",
		Email:    "support",
		Nickname: "support",
	}

	b, err = json.Marshal(jsonDataSup)
	if err != nil {
		s.NoError(err)
	}

	request, err = http.NewRequest("POST", fmt.Sprintf("%s/v1/register", s.ts.URL), bytes.NewBuffer(b))
	request.Header.Set("Content-Type", "application/json;")
	if err != nil {

		s.NoError(err)

	}

	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	defer response.Body.Close()

	s.Equal(200, response.StatusCode)

	///// Normal User
	jsonDataUser := &v1.RegisterRequest{
		Password: "password@1",
		Email:    "nordeasipvip@gmail.com",
		Nickname: "nordea",
		// Invite:   "alex",

	}

	b, err = json.Marshal(jsonDataUser)
	if err != nil {
		s.NoError(err)
	}

	s.email = jsonData.Email

	request, err = http.NewRequest("POST", fmt.Sprintf("%s/v1/register", s.ts.URL), bytes.NewBuffer(b))
	request.Header.Set("Content-Type", "application/json;")
	if err != nil {

		s.NoError(err)

	}
	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBodyr, err := io.ReadAll(response.Body)

	if err != nil {

		s.NoError(err)

	}

	var respr v1.Response

	err = json.Unmarshal(sbyteBodyr, &respr)
	if err != nil {

		s.NoError(err)

	}

	myMapr := respr.Data.(map[string]interface{})

	s.cid = myMapr["userid"].(string)
	s.code = myMapr["code"].(float64)

	defer response.Body.Close()

	s.Equal(200, response.StatusCode)

	s.db.Model(&model.User{}).Where("Nickname = ?", "admin").Updates(model.User{Role: "admin", Verify: true})
	s.db.Model(&model.User{}).Where("Nickname = ?", "support").Update("Role", "support")

	// LOGIN get Corect ADMIN JWT
	jsonDataLogin := &v1.LoginRequest{
		Password: "glassFAN01",
		Email:    "admin",
	}

	blogin, err := json.Marshal(jsonDataLogin)
	if err != nil {
		s.NoError(err)
	}

	request, err = http.NewRequest("POST", fmt.Sprintf("%s/v1/login", s.ts.URL), bytes.NewBuffer(blogin))
	request.Header.Set("Content-Type", "application/json;")
	if err != nil {

		s.NoError(err)

	}

	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err := io.ReadAll(response.Body)

	if err != nil {

		s.NoError(err)

	}
	var resplogin v1.Response

	err = json.Unmarshal(sbyteBody, &resplogin)
	if err != nil {

		s.NoError(err)

	}
	myMap := resplogin.Data.(map[string]interface{})

	s.adminbearer = myMap["accessToken"].(string)

	defer response.Body.Close()

	s.Equal(200, response.StatusCode)

	///////VERIFICATION by CODE ////////

	jsonDataVer := &v1.VerificationByCodeRequest{
		UserId: s.cid,
		Code:   int(s.code),
	}

	b, err = json.Marshal(jsonDataVer)
	if err != nil {
		s.NoError(err)
	}

	request, err = http.NewRequest("POST", fmt.Sprintf("%s/v1/verificationbycode", s.ts.URL), bytes.NewBuffer(b))
	request.Header.Set("Content-Type", "application/json;")
	if err != nil {

		s.NoError(err)

	}
	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}
	s.Equal(200, response.StatusCode)

	///////LOGIN//////
	jsonDataLoginUser := &v1.LoginRequest{
		Password: "password@1",
		Email:    "nordeasipvip@gmail.com",
	}

	b, err = json.Marshal(jsonDataLoginUser)
	if err != nil {
		s.NoError(err)
	}

	request, err = http.NewRequest("POST", fmt.Sprintf("%s/v1/login", s.ts.URL), bytes.NewBuffer(b))
	request.Header.Set("Content-Type", "application/json;")
	if err != nil {

		s.NoError(err)

	}
	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	sbyteBody, err = io.ReadAll(response.Body)

	if err != nil {

		s.NoError(err)

	}

	var resp v1.Response

	err = json.Unmarshal(sbyteBody, &resp)
	if err != nil {

		s.NoError(err)

	}

	myMap = resp.Data.(map[string]interface{})

	s.bearer = myMap["accessToken"].(string)
	s.cid = myMap["userid"].(string)

	s.Equal(200, response.StatusCode)

	///////UpdatePasword///////
	jsonDataPass := &v1.UpdatePasswordRequest{
		OldPassword: "password@1",
		NewPassword: "password@",
	}

	b, err = json.Marshal(jsonDataPass)
	if err != nil {
		s.NoError(err)
	}

	request, err = http.NewRequest("POST", fmt.Sprintf("%s/v1/updatepassword", s.ts.URL), bytes.NewBuffer(b))
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.bearer)
	if err != nil {

		s.NoError(err)

	}
	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	s.Equal(200, response.StatusCode)

	///////LOGIN2//////
	jsonDataLogin2 := &v1.LoginRequest{
		Password: "password@",
		Email:    "nordeasipvip@gmail.com",
	}

	b, err = json.Marshal(jsonDataLogin2)
	if err != nil {
		s.NoError(err)
	}

	request, err = http.NewRequest("POST", fmt.Sprintf("%s/v1/login", s.ts.URL), bytes.NewBuffer(b))
	request.Header.Set("Content-Type", "application/json;")
	if err != nil {

		s.NoError(err)

	}
	response, err = http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	s.Equal(200, response.StatusCode)

	s.Run("subTestUsers", func() {

		Users(s)

	})

}
