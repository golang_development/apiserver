package v1

type GetPaymenturlRequest struct {
	MerchantLogin string `json:"merchantlogin" example:"glsgo"`
	Description   string `json:"description" example:"payment for ChatGpt"`
	Amount        uint32 `json:"amount" example:"10000"`
	IsTest        string `json:"istest" example:"1"`
	Tariff        string `json:"tariff" example:"free"`
}

type GetInvoicesByClidResponce struct {
	Invoices []InvoicesByClidResponce `json:"invoices" `
}

type InvoicesByClidResponce struct {
	InvId         string
	UserId        string
	Description   string
	OutSum        float64
	PaymentMethod string
	IncSum        float64
	IncCurrLabel  string
	IsTest        bool
	EMail         string
	Fee           float64
	Tariff        string
}
