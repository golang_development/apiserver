package model

import "time"

type Creditcard struct {
	UserId    string `gorm:"unique;not null"`
	Num       string `gorm:"unique;not null"`
	Block     bool   `gorm:"default:false"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (m *Creditcard) TableName() string {
	return "creditcards"
}
