package repository

import (
	"gitlab.com/glass-go/back/apiserver/internal/model"
)

type CreditcardRepository interface {
	FirstById(id int64) (*model.Creditcard, error)
}

func NewCreditcardRepository(repository *Repository) CreditcardRepository {
	return &creditcardRepository{
		Repository: repository,
	}
}

type creditcardRepository struct {
	*Repository
}

func (r *creditcardRepository) FirstById(id int64) (*model.Creditcard, error) {
	var creditcard model.Creditcard
	// TODO: query db
	return &creditcard, nil
}
