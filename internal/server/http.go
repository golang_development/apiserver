package server

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	apiV1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/handler"
	"gitlab.com/glass-go/back/apiserver/internal/middleware"
	"gitlab.com/glass-go/back/apiserver/pkg/jwt"
	"gitlab.com/glass-go/back/apiserver/pkg/log"
	"gitlab.com/glass-go/back/apiserver/pkg/server/http"
)

func NewHTTPServer(
	logger *log.Logger,
	conf *viper.Viper,
	jwt *jwt.JWT,
	userHandler *handler.UserHandler,
	chattableHandler *handler.ChattableHandler,
	creditcardHandler *handler.CreditcardHandler,
	emailverificationHandler *handler.EmailverificationHandler,
	paymentHandler *handler.PaymentHandler,
	paymenthookHandler *handler.PaymenthookHandler,
	paymenturlHandler *handler.PaymenturlHandler,
	interviewskillHandler *handler.InterviewskillHandler,
	tafiffsHandler *handler.TariffHandler,
	clicksHandler *handler.ClickHandler,
	adminHandler *handler.AdminHandler,
) *http.Server {
	gin.SetMode(gin.DebugMode)
	s := http.NewServer(
		gin.Default(),
		logger,
		http.WithServerHost(conf.GetString("http.host")),
		http.WithServerPort(conf.GetInt("http.port")),
	)

	s.Use(
		middleware.CORSMiddleware(),
		middleware.ResponseLogMiddleware(logger),
		middleware.RequestLogMiddleware(logger),
		//middleware.SignMiddleware(log),
	)
	s.GET("/", func(ctx *gin.Context) {
		logger.WithContext(ctx).Info("hello")
		apiV1.HandleSuccess(ctx, map[string]interface{}{
			":)": "Thank you for using Glass-go apiserver!",
		})
	})

	v1 := s.Group("/v1")
	{

		noStrictAuthRouterAdmin := v1.Group("/admin").Use(middleware.NoStrictAuth(jwt, logger))
		{
			noStrictAuthRouterAdmin.GET("/allusers",adminHandler.GetAllUsers)
			noStrictAuthRouterAdmin.GET("/user",adminHandler.GetUserById)
			noStrictAuthRouterAdmin.GET("/clickstat",adminHandler.GetClickstat)	
			noStrictAuthRouterAdmin.GET("/clickstatbyid",adminHandler.GetClickstatById)	
		}
		// No route group has permission
		noAuthRouter := v1.Group("/")
		{
			noAuthRouter.POST("/register", userHandler.Register)
			noAuthRouter.POST("/login", userHandler.Login)
			noAuthRouter.POST("/chattableinternal", chattableHandler.InsertChattable)
			noAuthRouter.GET("/payment", paymentHandler.GetPayment)
			noAuthRouter.GET("/creditcard", creditcardHandler.GetCreditcard)
			noAuthRouter.GET("/emailverification/:cid", emailverificationHandler.SetEmailverification)
			noAuthRouter.POST("/verificationbycode", emailverificationHandler.VerificationByCode)
			noAuthRouter.POST("/forgotpassword", userHandler.ForgotPassword)
			noAuthRouter.GET("/paymenthook", paymenthookHandler.GetPaymenthook)
			noAuthRouter.GET("/notpaid", paymenthookHandler.NotPaid)
		}

		// Non-strict permission routing group
		noStrictAuthRouter := v1.Group("/").Use(middleware.NoStrictAuth(jwt, logger))
		{
			noStrictAuthRouter.GET("/user", userHandler.GetProfile)

			// #Tariffs
			noStrictAuthRouter.GET("/tariffs", tafiffsHandler.GetTariffs)
			// #Clicks
			noStrictAuthRouter.GET("/clicks", clicksHandler.GetClick)

			// #Admin

		}

		// Strict permission routing group
		strictAuthRouter := v1.Group("/").Use(middleware.StrictAuth(jwt, logger))
		{
			// #User
			strictAuthRouter.PUT("/user", userHandler.UpdateProfile)
			strictAuthRouter.POST("/chattable", chattableHandler.InsertChattable)
			strictAuthRouter.GET("/chattable", chattableHandler.GetChattable)
			strictAuthRouter.POST("/updatepassword", userHandler.UpdatePassword)
			strictAuthRouter.DELETE("/block/:cid", userHandler.BlockProfile)

			// #Payments
			strictAuthRouter.POST("/paymenturl", paymenturlHandler.GetPaymenturl)
			strictAuthRouter.GET("/getinvoicesbyclid", paymenturlHandler.GetInvoicesByClid)

			// #Interview
			strictAuthRouter.GET("/interviewsskill/:id", interviewskillHandler.GetInterviewskill)
			strictAuthRouter.POST("/insertinterviewsresult", interviewskillHandler.InserInterviewsresult)

		}
	}

	return s
}
