package model

import (
	"time"
)

type User struct {
	UserId              string  `gorm:"unique;not null"`
	Nickname            string  `gorm:"not null"`
	Password            string  `gorm:"not null"`
	Email               string  `gorm:"unique;not null"`
	Role                string  `gorm:"default:user"`
	Balance             float64 `gorm:"default:0"`
	Block               bool    `gorm:"default:false"`
	Verify              bool    `gorm:"default:false"`
	Code                int     `gorm:"not null"`
	Interview           string
	InterviewPermission bool `gorm:"default:false"`
	Tariff              uint
	DurationTill        time.Time
	Clicks              int `gorm:"default:0"`
	CreatedAt           time.Time
	UpdatedAt           time.Time
}

func (u *User) TableName() string {
	return "users"
}
