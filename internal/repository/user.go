package repository

import (
	"context"
	"errors"

	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/pkg/sendemail"
	"gorm.io/gorm"
)

type UserRepository interface {
	Create(ctx context.Context, user *model.User) error
	Update(ctx context.Context, user *model.User) error
	GetByID(ctx context.Context, id string) (*model.User, error)
	GetByEmail(ctx context.Context, email string) (*model.User, error)
	BlockProfile(ctx context.Context, cid string,block int) error
	UpdatePassword(ctx context.Context, userId string, newpassword string) error
	ForgotPassword(ctx context.Context, email string, newpassword string, newpasswordenc string) error
	CheckInvite(ctx context.Context, invcode string) (*model.Invite, bool, error)
	CheckEmail(ctx context.Context, email string) (bool, error)
}

func NewUserRepository(r *Repository) UserRepository {
	return &userRepository{
		Repository: r,
	}
}

type userRepository struct {
	*Repository
}

func (r *userRepository) CheckInvite(ctx context.Context, invcode string) (*model.Invite, bool, error) {

	var invite model.Invite

	var exists bool = false

	r.DB(ctx).Raw("SELECT NOT EXISTS(SELECT 1 FROM invite WHERE invcode = ? and block = 0)", invcode).Scan(&exists)

	if !exists {

		r.db.Model(&model.Invite{}).Where("invcode  = ?", invcode).Update("block", true)

		r.DB(ctx).Where("invcode = ? and block = 1", invcode).First(&invite)

	}

	return &invite, exists, nil
}

func (r *userRepository) Create(ctx context.Context, user *model.User) error {

	if err := r.DB(ctx).Create(user).Error; err != nil {
		return err
	}
	return nil
}

func (r *userRepository) Update(ctx context.Context, user *model.User) error {
	if err := r.DB(ctx).Save(user).Error; err != nil {
		return err
	}
	return nil
}

func (r *userRepository) GetByID(ctx context.Context, userId string) (*model.User, error) {
	var user model.User
	if err := r.DB(ctx).Where("user_id = ?", userId).First(&user).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, v1.ErrNotFound
		}
		return nil, err
	}
	return &user, nil
}

func (r *userRepository) CheckEmail(ctx context.Context, email string) (bool, error) {

	var exists bool = false

	r.DB(ctx).Raw("SELECT EXISTS(SELECT 1 FROM users WHERE email = ?)", email).Scan(&exists)

	return exists, nil
}

func (r *userRepository) GetByEmail(ctx context.Context, email string) (*model.User, error) {
	var user model.User

	if err := r.DB(ctx).Where("email = ? and block = 0", email).First(&user).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {

			return nil, nil
		}

		return nil, err
	}
	return &user, nil
}

func (r *userRepository) BlockProfile(ctx context.Context, cid string, block int) error {


	r.db.Model(&model.User{}).Where("user_id = ?", cid).Update("block", block)


	return nil
}

func (r *userRepository) UpdatePassword(ctx context.Context, userId string, newpassword string) error {

	r.db.Model(&model.User{}).Where("user_id = ?", userId).Update("password", newpassword)

	return nil
}

func (r *userRepository) ForgotPassword(ctx context.Context, email string, newpassword string, newpasswordenc string) error {

	r.db.Model(&model.User{}).Where("email = ?", email).Update("password", newpasswordenc)

	err := sendemail.SendEmail("https://mailer.glass-go.rest/forgotpassword", email, newpassword)
	if err != nil {

		return err
	}

	return nil
}
