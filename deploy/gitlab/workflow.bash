kubectl create namespace gitlab

helm install --namespace gitlab gitlab-runner -f deploy/gitlab/values.yaml gitlab/gitlab-runner
helm upgrade --namespace gitlab gitlab-runner -f deploy/gitlab/values.yaml gitlab/gitlab-runner