package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type InviteHandler struct {
	*Handler
	inviteService service.InviteService
}

func NewInviteHandler(handler *Handler, inviteService service.InviteService) *InviteHandler {
	return &InviteHandler{
		Handler:      handler,
		inviteService: inviteService,
	}
}

func (h *InviteHandler) GetInvite(ctx *gin.Context) {

}
