//go:build wireinject
// +build wireinject

package wire

import (
	"gitlab.com/glass-go/back/apiserver/internal/server"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
	"gitlab.com/glass-go/back/apiserver/pkg/app"
	"gitlab.com/glass-go/back/apiserver/pkg/log"
	"github.com/google/wire"
	"github.com/spf13/viper"
)

var taskSet = wire.NewSet(server.NewTask,repository.NewDB,repository.NewRepository)

// build App
func newApp(task *server.Task) *app.App {
	return app.NewApp(
		app.WithServer(task),
		app.WithName("glsgo-task"),
	)
}

func NewWire(*viper.Viper, *log.Logger) (*app.App, func(), error) {
	panic(wire.Build(
		taskSet,
		newApp,
	))
}
