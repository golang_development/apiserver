package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/service"
)

type UserHandler struct {
	*Handler
	userService service.UserService
}

func NewUserHandler(handler *Handler, userService service.UserService) *UserHandler {
	return &UserHandler{
		Handler:     handler,
		userService: userService,
	}
}

// Register godoc
//	@Summary	Register
//	@Schemes
//	@Description	Register
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Param			request	body		v1.RegisterRequest	true	"params"
//	@Success		200		{object}	v1.RegisterResponse
//	@Router			/register [post]
func (h *UserHandler) Register(ctx *gin.Context) {

	req := new(v1.RegisterRequest)
	if err := ctx.ShouldBindJSON(req); err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	user, err := h.userService.Register(ctx, req)

	if err != nil {
	
		v1.HandleError(ctx, http.StatusUnauthorized, err, nil)
		return
	}
	v1.HandleSuccess(ctx, v1.RegisterResponseData{
		// AccessToken: token.AccessToken,
		UserId: user.UserId,
		Code:   user.Code,
		Interview: user.Interview,
		InterviewPermission:  user.InterviewPermission,
		// Invite: user.Invite,
	})

}

// Login godoc
//	@Summary	Login
//	@Schemes
//	@Description
//	@Tags		Users
//	@Accept		json
//	@Produce	json
//	@Param		request	body		v1.LoginRequest	true	"params"
//	@Success	200		{object}	v1.LoginResponse
//	@Router		/login [post]
func (h *UserHandler) Login(ctx *gin.Context) {
	var req v1.LoginRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	user, err := h.userService.Login(ctx, &req)
	if err != nil {
		v1.HandleError(ctx, http.StatusUnauthorized, v1.ErrUnauthorized, nil)
		return
	}
	v1.HandleSuccess(ctx, v1.LoginResponseData{
		AccessToken: user.AccessToken,
		UserId:      user.UserId,
		Verify:      user.Verify,
		Interview: user.Interview,
		InterviewPermission: user.InterviewPermission,
	})
}

// GetProfile godoc
//	@Summary	GetProfile
//	@Schemes
//	@Description	GetProfile
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Security		Bearer
//	@Success		200	{object}	v1.GetProfileResponse
//	@Router			/user [get]
func (h *UserHandler) GetProfile(ctx *gin.Context) {
	userId := GetUserIdFromCtx(ctx)
	if userId == "" {
		v1.HandleError(ctx, http.StatusUnauthorized, v1.ErrUnauthorized, nil)
		return
	}

	user, err := h.userService.GetProfile(ctx, userId)
	if err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	v1.HandleSuccess(ctx, user)
}

// GetProfile godoc
//	@Summary	UpdateProfile
//	@Schemes
//	@Description	UpdateProfile
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Param			request	body	v1.UpdateProfileRequest	true	"params"
//	@Security		Bearer
//	@Success		200	{object}	v1.Response
//	@Router			/user [put]
func (h *UserHandler) UpdateProfile(ctx *gin.Context) {
	userId := GetUserIdFromCtx(ctx)

	var req v1.UpdateProfileRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	if err := h.userService.UpdateProfile(ctx, userId, &req); err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	v1.HandleSuccess(ctx, nil)
}

// Register godoc
//	@Summary	Block User (block=1 BLOCK block=0 UBLOCK )
//	@Schemes
//	@Description	Block User (block=1 BLOCK block=0 UBLOCK )
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Param			cid		path	string	true	"cid"
//	@Param			block	query	string	true	"0"
//	@Security		Bearer
//	@Success		200	{object}	v1.Response
//	@Router			/block/{cid} [delete]
func (h *UserHandler) BlockProfile(ctx *gin.Context) {

	block := ctx.Query("block")

	cid := ctx.Param("cid")

	bl , err := strconv.Atoi(block)
	if err != nil {

		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return		
	}

	if err := h.userService.BlockProfile(ctx, cid,bl); err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	v1.HandleSuccess(ctx, nil)
}

// GetProfile godoc
//	@Summary	UpdatePassword
//	@Schemes
//	@Description	UpdatePassword
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Param			request	body	v1.UpdatePasswordRequest	true	"params"
//	@Security		Bearer
//	@Success		200	{object}	v1.Response
//	@Router			/updatepassword [post]
func (h *UserHandler) UpdatePassword(ctx *gin.Context) {
	userId := GetUserIdFromCtx(ctx)

	var req v1.UpdatePasswordRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	if err := h.userService.UpdatePassword(ctx, userId, &req); err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	v1.HandleSuccess(ctx, nil)
}

// GetProfile godoc
//	@Summary	ForgotPassword (send new by email)
//	@Schemes
//	@Description	ForgotPassword (send new by email)
//	@Tags			Users
//	@Accept			json
//	@Produce		json
//	@Param			request	body		v1.ForgetPasswordRequest	true	"params"
//	@Success		200		{object}	v1.Response
//	@Router			/forgotpassword [post]
func (h *UserHandler) ForgotPassword(ctx *gin.Context) {

	var req v1.ForgetPasswordRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	if err := h.userService.ForgotPassword(ctx, &req); err != nil {
		v1.HandleError(ctx, http.StatusInternalServerError, v1.ErrInternalServerError, nil)
		return
	}

	v1.HandleSuccess(ctx, nil)
}
