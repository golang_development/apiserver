package repository

import (
	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/pkg/mlproviders/chatgpt"
	"gitlab.com/glass-go/back/apiserver/pkg/pushjob"
)

const (
	apiEndpoint = "https://api.openai.com/v1/chat/completions"
)

type ChattableRepository interface {
	GetAll(limit int) ([]model.Chattable, error)
	Insert(res *model.Chattable) (v1.ChatResponseData, error)
}

func NewChattableRepository(repository *Repository) ChattableRepository {
	return &chattableRepository{
		Repository: repository,
	}
}

type chattableRepository struct {
	*Repository
}

func (r *chattableRepository) GetAll(limit int) ([]model.Chattable, error) {
	var records []model.Chattable

	rows, err := r.db.Order("created_at desc").Limit(limit).Model(&model.Chattable{}).Rows()

	if err != nil {
		return records, err

	}
	defer rows.Close()

	for rows.Next() {

		var rec model.Chattable

		r.db.ScanRows(rows, &rec)

		records = append(records, rec)

	}
	// TODO: query db
	return records, nil
}

func (r *chattableRepository) Insert(rec *model.Chattable) (v1.ChatResponseData, error) {

	var ask string

	var res v1.ChatResponseData
	var chatrec model.Chattable

	count := int64(0)

	err := r.db.Model(&model.Chattable{}).Where("user_id", rec.UserId).Count(&count).Error
	if err != nil {

		return res, err
	}

	if count > 1 {

		r.db.Model(&model.Chattable{}).Where("user_id", rec.UserId).Last(&chatrec)

		ask = chatrec.Ask + " "

	}

	ask = ask + rec.Ask

	answer, err := chatgpt.Ask(r.rcl, apiEndpoint, ask)
	if err != nil {

		return res, err
	}

	rec.Answer = answer

	/// ROLLBACK
	// result := r.db.Create(rec)
	// if result.Error != nil {
	// 	return res, err
	// }

	

	pushQue := model.TaskQue{Sqlurls: r.tasqueue.Sqlurls, Tasqueue: r.tasqueue.Tasqueue}

	pushjob.PushIntoQue(rec, pushQue)

	res = v1.ChatResponseData{Answer: answer}

	return res, nil

}
