package service

import (
	"context"
	
	"gitlab.com/glass-go/back/apiserver/internal/model"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
)

type PaymenthookService interface {
	GetPaymenthook(ctx context.Context,payment *model.Invoice) error
	UpdateInvoice(ctx context.Context,payment *model.Invoice) error
}

func NewPaymenthookService(service *Service, paymenthookRepository repository.PaymenthookRepository) PaymenthookService {
	return &paymenthookService{
		Service:        service,
		paymenthookRepository: paymenthookRepository,
	}
}

type paymenthookService struct {
	*Service
	paymenthookRepository repository.PaymenthookRepository
}

func (s *paymenthookService) GetPaymenthook(ctx context.Context,payment *model.Invoice) (error) {

	return s.paymenthookRepository.InsertPayment(payment)
}


func (s *paymenthookService) UpdateInvoice(ctx context.Context,payment *model.Invoice) (error) {

	return s.paymenthookRepository.InsertPayment(payment)
}