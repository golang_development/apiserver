//go:build wireinject
// +build wireinject

package wire

import (
	"gitlab.com/glass-go/back/apiserver/internal/handler"
	"gitlab.com/glass-go/back/apiserver/internal/repository"
	"gitlab.com/glass-go/back/apiserver/internal/server"
	"gitlab.com/glass-go/back/apiserver/internal/service"
	"gitlab.com/glass-go/back/apiserver/pkg/app"
	"gitlab.com/glass-go/back/apiserver/pkg/helper/sid"
	"gitlab.com/glass-go/back/apiserver/pkg/jwt"
	"gitlab.com/glass-go/back/apiserver/pkg/log"
	"gitlab.com/glass-go/back/apiserver/pkg/server/http"
	"github.com/google/wire"
	"github.com/spf13/viper"
)

var repositorySet = wire.NewSet(
	
	repository.NewDB,
	repository.NewRedis,
	repository.NewResty,
	repository.NewRepository,
	repository.NewTransaction,
	repository.NewUserRepository,
	repository.NewChattableRepository,
	repository.NewPaymentRepository,
	repository.NewCreditcardRepository,
	repository.NewEmailverificationRepository,
	repository.NewPaymenthookRepository,
	repository.NewPaymenturlRepository,
	repository.NewRobo,
	repository.NewInterviewskillRepository,
	repository.NewTariffRepository,
	repository.NewClickRepository,
	repository.NewAdminRepository,
	repository.NewTasqueue,
	
)

var serviceSet = wire.NewSet(
	service.NewService,
	service.NewUserService,
	service.NewChattableService,
	service.NewPaymentService,
	service.NewCreditcardService,
	service.NewEmailverificationService,
	service.NewPaymenthookService,
	service.NewPaymenturlService,
	service.NewInterviewskillService,
	service.NewTariffService,
	service.NewClickService,
	service.NewAdminService,
		
)

var handlerSet = wire.NewSet(
	handler.NewHandler,
	handler.NewUserHandler,
	handler.NewChattableHandler,
	handler.NewPaymentHandler,
	handler.NewCreditcardHandler,
	handler.NewEmailverificationHandler,
	handler.NewPaymenthookHandler,
	handler.NewPaymenturlHandler,
	handler.NewInterviewskillHandler,
	handler.NewTariffHandler,
	handler.NewClickHandler,
	handler.NewAdminHandler,
)

var serverSet = wire.NewSet(
	server.NewHTTPServer,
	server.NewJob,
	server.NewTask,
)

// build App
func newApp(httpServer *http.Server, job *server.Job) *app.App {
	return app.NewApp(
		app.WithServer(httpServer, job),
		app.WithName("demo-server"),
	)
}

func NewWire(*viper.Viper, *log.Logger) (*app.App, func(), error) {

	panic(wire.Build(
		repositorySet,
		serviceSet,
		handlerSet,
		serverSet,
		sid.NewSid,
		jwt.NewJwt,
		newApp,
	))
}
