INTEGRATION_TEST_PATH?=./itest

.PHONY: init
init:
	go install github.com/google/wire/cmd/wire@latest
	go install github.com/golang/mock/mockgen@latest
	go install github.com/swaggo/swag/cmd/swag@latest

.PHONY: bootstrap
bootstrap:
	cd ./deploy/docker-compose && docker compose up -d && cd ../../
	go run ./cmd/migration
	nunu run ./cmd/server

.PHONY: mock
mock:
	mockgen -source=internal/service/user.go -destination test/mocks/service/user.go
	mockgen -source=internal/repository/user.go -destination test/mocks/repository/user.go
	mockgen -source=internal/repository/repository.go -destination test/mocks/repository/repository.go

.PHONY: test
test:
	go test -coverpkg=./internal/handler,./internal/service,./internal/repository -coverprofile=./coverage.out ./test/server/...
	# go test -coverpkg=./internal/handler -coverprofile=./coverage.out ./test/server/...
	go tool cover -html=./coverage.out -o coverage.html

.PHONY: build
build:
	go build -ldflags="-s -w" -o ./bin/server ./cmd/server


# LOCAL QUE
.PHONY: docker-que-local
docker-que-local:

	docker buildx build -t "remotejob/quetask:v0.0.14-local" -f deploy/build/Dockerfile.que --build-arg APP_CONF=config/local.yml --platform linux/amd64,linux/arm64 --push .


# QUE
.PHONY: docker-que-test
docker-que-test:

	docker buildx build -t "remotejob/quetask:v0.0.1-test" -f deploy/build/Dockerfile.que --build-arg APP_CONF=config/test.yml --platform linux/amd64,linux/arm64 --push .


PHONY: docker-que-prod
docker-que-prod:

	docker buildx build -t "remotejob/quetask:v0.0.2-prod" -f deploy/build/Dockerfile.que --build-arg APP_CONF=config/prod.yml --platform linux/amd64,linux/arm64 --push .


# TASK
.PHONY: docker-task-test
docker-task-test:

	docker buildx build -t "remotejob/crontask:v0.0.11-test" -f deploy/build/Dockerfile.task --build-arg APP_CONF=config/test.yml --platform linux/amd64,linux/arm64 --push .


PHONY: docker-task-prod
docker-task-prod:

	docker buildx build -t "remotejob/crontask:v0.0.2-prod" -f deploy/build/Dockerfile.task --build-arg APP_CONF=config/prod.yml --platform linux/amd64,linux/arm64 --push .


.PHONY: docker-test
docker-test:

	docker buildx build -t "remotejob/apiserver:v0.0.78-test" -f deploy/build/Dockerfile.k8s --build-arg APP_CONF=config/test.yml --platform linux/amd64,linux/arm64 --push .

.PHONY: docker-prod
docker-prod:

	docker buildx build -t "remotejob/apiserver:v0.0.51-prod" -f deploy/build/Dockerfile.k8s --build-arg APP_CONF=config/prod.yml --platform linux/amd64,linux/arm64 --push .


.PHONY: swag
swag:
	swag fmt
	swag init --instanceName apiserver --markdownFiles ./docs -g cmd/server/main.go -o /mnt/seconddisk/repos/gitlab.com/golang_development/stand-alone-swagger/docs

.PHONY: itest
itest:
	go test -tags=integration $(INTEGRATION_TEST_PATH) -count=1 -v 


# GITLAB

PHONY: docker-gitlab-test
docker-gitlab-test:

	docker buildx build -t "registry.gitlab.com/glass-go/back/apiserver:v0.0.0-test" -f deploy/build/Dockerfile.k8s --build-arg APP_CONF=config/test.yml --platform linux/amd64,linux/arm64 --push .


	