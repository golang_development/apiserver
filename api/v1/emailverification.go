package v1
type VerificationByCodeRequest struct {
	UserId string `json:"userid"  example:"AjWXfLwVAT"`
	Code   int    `json:"code" example:"12345"`
}

