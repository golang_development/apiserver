package itest_test

import (
	"fmt"
	"net/http"
)

func BlockProfile(s *e2eTestSuite) {

	request, err := http.NewRequest("DELETE", fmt.Sprintf("%s/v1/block/"+s.cid, s.ts.URL), nil)
	request.Header.Set("Content-Type", "application/json;")
	// request.Header.Set("Authorization", "Bearer "+s.bearer)
	if err != nil {

		s.NoError(err)

	}
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	s.Equal(200, response.StatusCode)

	// sbyteBody, err := io.ReadAll(response.Body)
	// if err != nil {
	// 	s.NoError(err)

	// }

	// log.Println(string(sbyteBody))

	// s.Run("subChattableUsers", func() {

	// 	Chattable(s)

	// })

}
