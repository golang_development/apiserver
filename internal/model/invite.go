package model

import (
	"time"
)

type Invite struct {
	UserId     string
	Invcode    string `gorm:"unique;not null"`
	Block      bool   `gorm:"default:false"`
	Item       string
	Permission bool   `gorm:"default:true"`
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

func (m *Invite) TableName() string {
	return "invite"
}
