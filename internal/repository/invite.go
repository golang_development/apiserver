package repository

import (
	"gitlab.com/glass-go/back/apiserver/internal/model"
)

type InviteRepository interface {
	FirstById(id int64) (*model.Invite, error)
}

func NewInviteRepository(repository *Repository) InviteRepository {
	return &inviteRepository{
		Repository: repository,
	}
}

type inviteRepository struct {
	*Repository
}

func (r *inviteRepository) FirstById(id int64) (*model.Invite, error) {
	var invite model.Invite
	// TODO: query db
	return &invite, nil
}
