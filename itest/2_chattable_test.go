package itest_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
	v1 "gitlab.com/glass-go/back/apiserver/api/v1"
)

func Chattable(s *e2eTestSuite) {

	var fakeHacks []string

	for i := 0; i < 4; i++ {

		gofakeit.Seed(0)
		phrase := gofakeit.HackerPhrase()
		fakeHacks = append(fakeHacks, phrase)

	}

	testCases := []struct {
		name string
		args *v1.ChattableInsert
	}{

		// {"firstAsk", &v1.ChattableInsert{UserId: s.cid, Mlmodel: "chatgpt", Ask: fakeHacks[0]}},
		// {"secondAsk", &v1.ChattableInsert{UserId: s.cid, Mlmodel: "chatgpt", Ask: fakeHacks[1]}},
		// // {"Ask3", &v1.ChattableInsert{UserId: s.cid, Mlmodel: "chatgpt", Ask: fakeHacks[2]}},
		// {"Ask4", &v1.ChattableInsert{UserId: s.cid, Mlmodel: "chatgpt", Ask: fakeHacks[3]}},
	}

	for _, tc := range testCases {

		s.T().Run(tc.name, func(t *testing.T) {

			b, err := json.Marshal(tc.args)
			if err != nil {
				s.NoError(err)
			}

			request, err := http.NewRequest("POST", fmt.Sprintf("%s/v1/chattable", s.ts.URL), bytes.NewBuffer(b))
			request.Header.Set("Content-Type", "application/json;")
			request.Header.Set("Authorization", "Bearer "+s.bearer)
			if err != nil {

				s.NoError(err)

			}
			response, err := http.DefaultClient.Do(request)
			if err != nil {
				s.NoError(err)

			}

			defer response.Body.Close()

			s.Equal(200, response.StatusCode)

		})

	}

	request, err := http.NewRequest("GET", fmt.Sprintf("%s/v1/chattable?limit=3", s.ts.URL), nil)
	request.Header.Set("Content-Type", "application/json;")
	request.Header.Set("Authorization", "Bearer "+s.bearer)

	if err != nil {

		s.NoError(err)

	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		s.NoError(err)

	}

	defer response.Body.Close()

	sbyteBody, err := io.ReadAll(response.Body)

	log.Println(string(sbyteBody))

	s.Equal(200, response.StatusCode)

	s.Run("subPaymentHook", func() {

		Paymneturl(s)

	})

}
