package server

import (
	"context"
	"log/slog"
	"os"
	"os/signal"
	"time"

	"gitlab.com/glass-go/back/apiserver/pkg/log"
	"gitlab.com/glass-go/back/apiserver/pkg/tasks"
	"gorm.io/gorm"

	"github.com/kalbhor/tasqueue/v2"
	rb "github.com/kalbhor/tasqueue/v2/brokers/redis"
	rr "github.com/kalbhor/tasqueue/v2/results/redis"
	"github.com/spf13/viper"
)

type Job struct {
	log  *log.Logger
	conf *viper.Viper
	db   *gorm.DB
	srv  *tasqueue.Server
}

func NewJob(log *log.Logger, conf *viper.Viper, db *gorm.DB) *Job {
	return &Job{
		log:  log,
		conf: conf,
		db:   db,
	}
}
func (j *Job) Start(ctx context.Context) error {

	ctx, _ = signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	lo := slog.Default()

	rdbaddr := j.conf.GetString("data.redis.addr")
	rdbpass := j.conf.GetString("data.redis.password")

	lo.Info("rdbaddres", "add", rdbaddr)
	srv, err := tasqueue.NewServer(tasqueue.ServerOpts{
		Broker: rb.New(rb.Options{
			Addrs:    []string{rdbaddr},
			Password: rdbpass,
			DB:       0,
		}, lo),
		Results: rr.New(rr.Options{
			Addrs:      []string{rdbaddr},
			Password:   rdbpass,
			DB:         0,
			MetaExpiry: time.Second * 60,
		}, lo),
		Logger: lo.Handler(),
	})
	if err != nil {
		lo.Error(err.Error())
	}

	j.srv = srv

	err = srv.RegisterTask("insertdb", tasks.InsertDBProcessor, tasqueue.TaskOpts{Concurrency: 1})
	if err != nil {
		lo.Error(err.Error())
	}

	j.srv.Start(ctx)

	return nil
}
func (j *Job) Stop(ctx context.Context) error {
	return nil
}
