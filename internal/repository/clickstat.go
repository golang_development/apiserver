package repository

import (

	"gitlab.com/glass-go/back/apiserver/internal/model"
)

type ClickstatRepository interface {
	FirstById(id int64) (*model.Clickstat, error)
	// GetAllClickstat(ctx context.Context) (*v1.GetClickstatResponce, error)
}

func NewClickstatRepository(repository *Repository) ClickstatRepository {
	return &clickstatRepository{
		Repository: repository,
	}
}

type clickstatRepository struct {
	*Repository
}

func (r *clickstatRepository) FirstById(id int64) (*model.Clickstat, error) {
	var clickstat model.Clickstat
	// TODO: query db
	return &clickstat, nil
}

// func (r *clickstatRepository) GetAllClickstat(ctx context.Context) (*v1.GetClickstatResponce, error) {
// 	var res v1.GetClickstatResponce

// 	var clickstat []model.Clickstat

// 	var clicksarr []v1.ClickstatResponce

// 	r.DB(ctx).Where("1 == 1").Find(&clickstat)

// 	for _, click := range clickstat {

// 		out := v1.ClickstatResponce{UserId: click.UserId, Clicks: click.Clicks, ClicksLimit: click.ClicksLimit}
// 		clicksarr = append(clicksarr, out)

// 	}

// 	res.Clickstat = clicksarr

// 	return &res, nil

// }
