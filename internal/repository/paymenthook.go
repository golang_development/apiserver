package repository

import (
	"log"

	"time"

	"gitlab.com/glass-go/back/apiserver/internal/model"
)

type PaymenthookRepository interface {
	// FirstById(cid string) error
	InsertPayment(payment *model.Invoice) error
	NotPaid(payment *model.Invoice) error
}

func NewPaymenthookRepository(repository *Repository) PaymenthookRepository {
	return &paymenthookRepository{
		Repository: repository,
	}
}

type paymenthookRepository struct {
	*Repository
}

func (r *paymenthookRepository) NotPaid(payment *model.Invoice) error {

	var invoice model.Invoice

	r.db.First(&invoice, payment.InvId)

	log.Println("!!!NOtPaid", invoice)

	invoice.InvId = payment.InvId
	invoice.IncSum = payment.IncSum
	invoice.IncCurrLabel = payment.IncCurrLabel
	invoice.PaymentMethod = payment.PaymentMethod
	invoice.Crc = payment.Crc
	invoice.OutSum = payment.OutSum
	invoice.IsTest = payment.IsTest
	invoice.EMail = payment.EMail
	invoice.Fee = payment.Fee

	r.db.Save(invoice)

	return nil
}

func (r *paymenthookRepository) InsertPayment(payment *model.Invoice) error {

	var invoice model.Invoice

	r.db.First(&invoice, payment.InvId)

	// sha := sha512.New()
	// _, err := sha.Write([]byte(strings.Join([]string{
	// 	strconv.ParseFloat(invoice.OutSum),
	// 	invoice.InvId,
	// 	r.robo.Password2,
	// }, ":")))
	// if err != nil {
	// 	return err
	// }

	// hash := strings.ToUpper(hex.EncodeToString(sha.Sum(nil)))

	// if hash == payment.Crc {

	// 	log.Println("!!!Check Payment OK!!!", hash)

	// }

	invoice.InvId = payment.InvId
	invoice.IncSum = payment.IncSum
	invoice.IncCurrLabel = payment.IncCurrLabel
	invoice.PaymentMethod = payment.PaymentMethod
	// invoice.Crc = payment.Crc
	invoice.OutSum = payment.OutSum
	invoice.IsTest = payment.IsTest
	invoice.EMail = payment.EMail
	invoice.Fee = payment.Fee

	r.db.Save(invoice)

	log.Println("!!!!TARIFFF", invoice.Tariff)

	var tariff model.Tariff

	r.db.First(&tariff, invoice.Tariff)

	start := time.Now()

	days := tariff.Duration
	till := start.AddDate(0, days, 0)

	r.db.Where("user_id = ?", invoice.UserId).Updates(model.User{Balance: float64(invoice.IncSum), Tariff: invoice.Tariff, DurationTill: till, Clicks: tariff.Clicklimit})

	return nil
}
